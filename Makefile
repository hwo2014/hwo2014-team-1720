.PHONY: all test run

TRACK ?= keimola
NUMCARS ?= 4
SERVER ?= hakkinen.helloworldopen.com
PASSWD ?= passu42

all: test

test:
	python -m doctest python/main.py

flake8:
	flake8 --ignore=E501 python/main.py python/Dijkstra.py

run:
	./run $(SERVER) 8091 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

runcreate:
	./run $(SERVER) 8091 create $(TRACK) $(PASSWD) $(NUMCARS) | tee run-$(shell date "+%Y%m%d-%H%M%S").log

runjoin:
	./run $(SERVER) 8091 join $(TRACK) $(PASSWD) $(NUMCARS) | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_keimola:
	./run $(SERVER) 8091 create keimola $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_germany:
	./run $(SERVER) 8091 create germany $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_usa:
	./run $(SERVER) 8091 create usa $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_france:
	./run $(SERVER) 8091 create france $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_elaintarha:
	./run $(SERVER) 8091 create elaeintarha $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_imola:
	./run $(SERVER) 8091 create imola $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_england:
	./run $(SERVER) 8091 create england $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_suzuka:
	./run $(SERVER) 8091 create suzuka $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

run_pentag:
	./run $(SERVER) 8091 create pentag $(PASSWD) 1 | tee run-$(shell date "+%Y%m%d-%H%M%S").log

from __future__ import division
from __future__ import print_function
import math
import timer


def curvate(x, a, b, c, d):
    """ This is the main function to calculate the curve

    Takes x position and paramteres as separate values
    """
    return (a - b) / (math.exp(-c * (x - d)) + 1) + b


def curvate_with_params(x, param):
    """ This calls the curve functions.
    Takes x position and paramteres as tuple or list
    """
    return curvate(x, *param)


def calcError(set1, set2):
    """ Calculates the error between set of values
    """
    ll = min(len(set1), len(set2))

    e = 0
    for i in xrange(ll):
        e += (set1[i] - set2[i]) ** 2

    return e


def diffError(func, values, params):
    """ Calculates new curve and then error to the original one
    """
    est = []

    for i, val in enumerate(values):
        est.append(func(i, params))

    return calcError(values, est)


def findFit(func, values, params, index, fitrate=0.0001, maxtry=10000):
    """ Finds better fit for specified input parameter
    """
    arr1 = list(params)
    arr2 = list(params)
    diff = fitrate

    error_min = 9999999
    diff_min = 0
    eplus = 0
    eneg = 0
    #print ("pre", arr1, index)
    cnt = 0
    while cnt < maxtry:
        arr1[index] = params[index] + diff
        arr2[index] = params[index] - diff

        #print ("b\n", arr1, "\n", arr2)
        eplus = diffError(func, values, arr1)
        eneg = diffError(func, values, arr2)
        tmp = min(eplus, eneg)
        if tmp < error_min:
            error_min = tmp
            if error_min == eplus:
                diff_min = diff
            else:
                diff_min = -diff
        else:
            break

        diff += fitrate
        cnt += 1

    return (error_min, diff_min)


def findCurve(func, values, params, fitrate):
    """ Finds best fit for curve with specified values and parameters
    """
    res = list(params)
    error_init = diffError(func, values, res)

    for index, _ in enumerate(res):
        (error_val, diff_val) = findFit(func, values, res, index, fitrate)
        if error_val < error_init:
            res[index] += diff_val
            error_init = error_val

    error_final = diffError(func, values, res)
    return (res, error_final)


def findBestFit(values, fitrate=0.001, default_values=[], change_rate=0.1):
    if not values:
        return None

    """
    a = max
    b = min
    c = rate of change
    d = midpoint
    """
    if not default_values:
        params = (max(values), min(angle_values), change_rate, sum(values) / len(values))
    else:
        params = default_values
    #print (params)

    (res, final_error) = findCurve(curvate_with_params, angle_values, params, fitrate)

    return res

if __name__ == '__main__':
    #angle_values = [0.019533343301639763, 0.09194129018865652, 0.1561242716469643, 0.21221748464377532, 0.260429020630404, 0.30103039349853633, 0.3343473322430756, 0.3607509101392991, 0.38064907156590205, 0.3944786073307542, 0.4026976195528292, 0.4057785078798848, 0.40420150012381584, 0.39844874231018573, 0.7654331139153536, 1.4643481657675697, 2.4548986796446735, 3.6976807288207496, 5.154514416202512, 6.788730434854368, 8.565411975229088, 10.451593829137686, 12.416420814238094, 14.431267866803587, 16.469824327269293, 18.50814507532626, 20.52467126211665, 22.500223439510197, 24.417969903742193, 26.263373056154993, 28.024116540712757, 29.690015849636602, 31.252914998163668, 32.70657176021565, 34.04653383170718, 35.27000815023924, 36.37572545176571, 37.363801989085744, 38.23560017612096, 38.99358975811773, 39.641210943217494, 40.18274076711287, 40.62316380141319, 40.96804815934543, 41.22342760079168, 41.3956903935056]
    angle_values = [0.028287413544977887, 0.06883318368749836, 0.10457801561127039, 0.1356124665056878, 0.16206685292469017, 0.18410590879350552, 0.20192466912592155, 0.21574375618125632, 0.6512072420671137, 1.454721226218715, 2.5757670942259914, 3.967388656641094, 5.58656805028932, 7.394577368649276, 9.35733306098536, 11.445792239592311, 13.633504195138567, 15.827754666361765, 18.000582650965214, 20.128882471853814, 22.194337140822856, 24.18336681454016, 26.087116657523225, 27.901518624559476, 29.62492049541076, 31.195235748406343, 32.60828542078295, 33.863617705665945, 34.96430004610689, 35.91675410569987, 36.73065315835378, 37.418910239408596, 37.99593517250283, 38.43038238968847, 38.74145931264695, 38.947620737919, 39.066458975088935, 39.09245810980821, 39.02246725600033, 38.85547945992476]

    #params = [39.7, -1.6, 0.226, 19.7]
    fitrates = [1, 0.1, 0.01, 0.001, 0.0001]
    for fitrate in fitrates:
        t1 = timer.Timer()
        res = findBestFit(angle_values, fitrate)
        print (res)
        t1.end(1 / 60)
        print (t1, "\n")

    print ("---\n")

    fitrates = [1, 0.1, 0.01, 0.001, 0.0001]
    res = []
    for fitrate in fitrates:
        t1 = timer.Timer()
        res = findBestFit(angle_values, fitrate, res)
        print (res)
        t1.end(1 / 60)
        print (t1, "\n")

    """
    # Try what we can get out of the function
    tmp = []
    for x in xrange(50):
        tmp.append(curvate_with_params(x, res))

    print (tmp)
    """

    """
    t2 = timer.Timer()
    print (findBestFit(angle_values2))
    t2.end(1 / 60)

    print (t2)
    """

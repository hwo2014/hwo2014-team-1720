from __future__ import division
from __future__ import print_function
import Dijkstra
import json
import math
import operator
import os
import random
import re
import socket
import socks
import sys
import time
import timer
import traceback


def clamp(number, num_min, num_max):
    return max(min(num_max, number), num_min)


class Ecar42Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.track = {}
        self.car_name = None
        self.car_color = None
        self.car_angle = 0.0
        self.car_angle_prev = 0.0

        self.message_sent_tick = -1

        self.cars = {}
        self.car_pos = {}
        self.car_racepos = 0
        self.opponents = {}
        self.car_dimensions = {}
        self.car_mass = 0
        self.analyze_done = False

        self.last_switch = -1

        self.air_resistance_constant = 0

        self.learning = True
        self.learning_slowdown = False

        self.learning_throttle = 1.0
        self.learning_speed_stack = []
        self.centrifugal = 0
        self.centrifugal_stack = []
        self.acceleration_initial = 0
        self.acceleration_initial_turbo = 0
        self.friction_constant = 0
        self.slowdown = 0
        self.crash_dec_cnt = 0.01

        self.name_extras = [u'akuten', u'kranskaus', u'mote', u'kopoti']
        self.message_sent_msg = ""
        self.acceleration_initial = 0
        self.prev_change = 0
        self.message_stack = []
        self.moment_stack = []

        # FIXME Figure out best ratios
        self.SAFE_LOW = 0.5  # Percentage of lower limit in centrifugal force

        # FIXME Figure out best value
        #self.DEC_COUNT_MUL = 10
        self.DEC_COUNT_MUL = 5

        self.MAX_TARGET_SPEED = 20  # FIXME, hardcoded
        self.GRAVITY = 9.80665

        self.TURBO_SAFETY_FACTOR = 3.0  # FIXME Figure out good value for this

        self.corner_speed = 1.0  # Some default
        self.corner_change_time = 0
        self.crash_count = 0
        self.crashes_per_lap = 0
        self.CRASHES_PER_LAP_LIMIT = 4  # Limit for crash count per lap
        self.lap = 0
        self.lap_time = 0.0
        self.crash_shown = False

        self.REACH_DISTANCE = 10  # Distance from where we can reach another car, used as speed * REACH_DISTANCE

        self.STACK_SIZE = 5
        self.end_pos_path = "0A"

        self.speed = {'speed': 0}
        self.prev_speed = {'speed': 0}
        self.speed_stack = []
        self.tick_stack = []
        self.speed_stack_rates = []
        self.prev_stat = {}
        self.curr_stat = {}

        self.curr_tick = 0
        self.prev_tick = 0
        self.acceleration = 0

        self.throttle_amount = 0

        self.track_sections = {}

        self.crashed = False
        self.name_printed = False

        # Use environment variable CAR_VERBOSE to detect verbose
        if "CAR_VERBOSE" in os.environ:
            dbg = os.environ["CAR_VERBOSE"].lower()
            self.verbose = (dbg == 'yes') or (dbg == 'on') or (dbg == '1')
        else:
            self.verbose = True  # FIXME False for production code

        if "CAR_VISUAL" in os.environ:
            dbg = os.environ["CAR_VISUAL"].lower()
            self.visual = (dbg == 'yes') or (dbg == 'on') or (dbg == '1')
            if self.visual:
                self.verbose = False
                self.debug = False
        else:
            self.visual = False

        # Use environment variable CAR_DEBUG to detect debugging
        if "CAR_DEBUG" in os.environ:
            dbg = os.environ["CAR_DEBUG"].lower()
            self.debug = (dbg == 'yes') or (dbg == 'on') or (dbg == '1')
        else:
            self.debug = False  # Use environment variable CAR_DEBUG

        if "CAR_LOG" in os.environ:
            dbg = os.environ["CAR_LOG"].lower()
            self.write_log = (dbg == 'yes') or (dbg == 'on') or (dbg == '1')
        else:
            self.write_log = False

        self.log_file = 'log_%s.json' % (time.strftime('%Y-%m-%d_%H-%M-%S'))
        self.log = []
        self.track_path_tree = {}
        self.shortest_path = {}

        self.turbo_start = -1
        self.turbo_end = -1
        self.turbo_on = False
        self.turbo_max_throttle = 1.0

        self.race_laps = -1
        self.race_duration = -1
        self.race_max_laptime = -1
        self.race_quickrace = False
        self.qualifying = False

    def msg(self, msg_type, data):
        self.send(
            json.dumps(
                {"msgType": msg_type,
                 "gameTick": self.curr_tick,
                 "data": data}
            )
        )

    def send(self, msg):
        """ Send the message to the socket.
        For testing purposes if sockes is None, bail out.
        """
        if self.socket is None:
            return
        if self.curr_tick > 0 and self.message_sent_tick == self.curr_tick:
            print ("ERROR: trying to send another msg on tick %s: %s. Already sent: %s" % (self.curr_tick, msg, self.message_sent_msg))
            #if self.debug:
            print (self.message_stack[-self.STACK_SIZE:])
            return

        self.message_sent_tick = self.curr_tick
        self.message_sent_msg = msg
        self.socket.sendall(msg + "\n")

    def join(self):
        """ Join a race
        """
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        """ Send throttle message
        """
        self.throttle_amount = throttle
        self.msg("throttle", throttle)
        if self.write_log:
            self.log.append({'msgType': 'throttleData', 'data': {'throttle': throttle, 'gameTick': self.curr_tick}})

    def switch_lane(self, direction):
        """ Send switch lane message
        """
        self.msg("switchLane", direction)

    def ping(self):
        """ Ping server
        """
        self.msg("ping", {})

    def enable_turbo(self, msg="Enable turbo, plz"):
        """ Enable turbo mode
        """
        if self.verbose:
            print ("GOT TURBO")
        self.msg("turbo", msg)
        self.turbo_start = -1

    def create_race(self, data):
        """ Create a race
        """
        return self.msg(
            "createRace",
            {"botId":
                {
                    "name": ("%s-%s" % (self.name, random.choice(self.name_extras)))[:20],
                    "key": self.key
                },
             "trackName": data["track_name"],
             "password": data["pass"],
             "carCount": int(data["cars"])
             })

    def join_race(self, data):
        """ Join to a race
        """
        #"name": "%s-%s-%s" % (self.name, random.choice(self.name_extras), random.randint(100, 1000)),
        #"name": "%s-%s" % (random.choice(self.name_extras), random.randint(100, 1000)),
        return self.msg(
            "joinRace",
            {"botId":
                {
                    "name": ("%s-%s%s" % (self.name, random.choice(self.name_extras), random.randint(1, 10)))[:20],
                    "key": self.key
                },
             "trackName": data["track_name"],
             "password": data["pass"],
             "carCount": int(data["cars"])
             })

    def run(self, data):
        """ Run the bot
        """
        if "start" in data:
            if data["start"] == "create":
                self.create_race(data)
            else:
                self.join_race(data)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")

    def on_your_car(self, data):
        print("Car info")
        self.car_name = data.get("name", None)
        self.car_color = data.get("color", None)

    def validate_car(self, car):
        """ Validates the car data structure got from server

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.validate_car({"id": {"color": "red", "name": "Schumacher"}})
        True
        >>> obj.validate_car({"id"})
        False
        >>> obj.validate_car({"id": {}})
        False
        >>> obj.validate_car({"id": {"color": "red"}})
        False
        >>> obj.validate_car({"id": {"color": 42, "name": "Random"}})
        True
        >>> obj.validate_car({"id": {"color": 42, "name": "Random", "a": "b"}})
        True
        >>> obj.validate_car(None)
        False
        """
        if type(car) != dict:
            return False
        if "id" not in car:
            return False
        if type(car["id"]) != dict:
            return False
        if "color" not in car["id"]:
            return False
        if "name" not in car["id"]:
            return False
        return True

    def isnum(self, val):
        """ Check if parameter is numeric

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.isnum(1)
        True
        >>> obj.isnum(1L)
        True
        >>> obj.isnum(0)
        True
        >>> obj.isnum(1.2)
        True
        >>> obj.isnum("1.2")
        False
        >>> obj.isnum({})
        False
        >>> obj.isnum(None)
        False
        """
        return isinstance(val, (int, long, float))

    def parse_dimensions(self, car):
        """ Parses dimension data in car to get car size

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.parse_dimensions({"dimensions": {}})
        False
        >>> obj.parse_dimensions({"dimensions": {"length": 12, "width": 42, "guideFlagPosition": 10}})
        True
        >>> obj.parse_dimensions(None)
        False
        >>> obj.parse_dimensions({"dimensions": {"length": 12.0, "width": 42, "guideFlagPosition": 10L}})
        True
        >>> obj.car_dimensions["length"]
        12.0
        >>> obj.car_dimensions["width"]
        42
        >>> obj.car_dimensions["guideFlagPosition"]
        10L
        >>> obj.parse_dimensions({"dimensions": {"length": 12, "width": "42", "guideFlagPosition": 10}})
        False
        >>> obj.parse_dimensions({"dimensions"})
        False
        """
        if type(car) != dict:
            return False
        if "dimensions" not in car:
            return False
        if "length" not in car["dimensions"] or not self.isnum(car["dimensions"]["length"]):
            return False
        if "width" not in car["dimensions"] or not self.isnum(car["dimensions"]["width"]):
            return False
        if "guideFlagPosition" not in car["dimensions"] or not self.isnum(car["dimensions"]["guideFlagPosition"]):
            return False
        self.car_dimensions = car["dimensions"]
        return True

    def parse_car_pos(self, car):
        """ Parses the car position, gets essential info

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.parse_car_pos({"piecePosition": {"pieceIndex": 1}, "angle": 0})
        (0, {'pieceIndex': 1})
        >>> obj.parse_car_pos({"piecePosition": 99, "angle": 42})
        (0.0, {})
        >>> obj.parse_car_pos({"piecePosition": {}, "angle": 42})
        (42, {})
        >>> obj.parse_car_pos({"piecePosition"})
        (0.0, {})
        >>> obj.parse_car_pos({"piecePosition": 2})
        (0.0, {})
        >>> obj.parse_car_pos(None)
        (0.0, {})
        """

        if type(car) != dict:
            return (0.0, {})
        if "piecePosition" not in car or type(car["piecePosition"]) != dict:
            return (0.0, {})
        if "angle" not in car:
            return (0.0, {})

        return (car["angle"], car["piecePosition"])

    def parse_car_data(self, cars):
        """ Parses the car data from server feed info

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.parse_car_data(None)
        False
        >>> obj.parse_car_data([])
        False
        >>> obj.car_color = "red"
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}}])
        False
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}},{"id": {"color": "red", "name": "mycar"}, "dimensions": {"length": 1, "width": 2, "guideFlagPosition": 3}}])
        True
        >>> obj.car_dimensions["length"] == 1
        True
        >>> obj.car_dimensions["width"] == 2
        True
        >>> obj.car_dimensions["guideFlagPosition"] == 3
        True
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 5, "inPieceDistance": 2}, "angle": 44},{"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 4, "inPieceDistance": 20}, "angle": 124}])
        True
        >>> obj.car_angle
        124
        >>> obj.car_pos["pieceIndex"]
        4
        >>> obj.car_pos["inPieceDistance"]
        20
        >>> "blue" in obj.opponents
        True
        >>> obj.opponents["blue"]["angle"]
        44
        >>> obj.opponents["blue"]["pos"]["pieceIndex"]
        5
        >>> obj.opponents["blue"]["pos"]["inPieceDistance"]
        2
        """
        if type(cars) != list:
            return False
        res = False
        for car in cars:
            if not self.validate_car(car):
                continue
            if car["id"]["color"] == self.car_color:
                self.parse_dimensions(car)
                self.car_pos_prev = self.car_pos
                self.car_angle_prev = self.car_angle
                (self.car_angle, self.car_pos) = self.parse_car_pos(car)
                res = True
            else:
                self.opponents[car["id"]["color"]] = {}
                (angle, pos) = self.parse_car_pos(car)
                self.opponents[car["id"]["color"]]["angle"] = angle
                self.opponents[car["id"]["color"]]["pos"] = pos

        return res

    def parse_init_data(self, data):
        """ Parses initialization data message

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 100, "angle": 22.5, "switch": True}, {"radius": 100, "angle": 22.5} ]}
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 0 }, {"index":2, "distanceFromCenter": 10 }]
        >>> tmp = {"race": {}}
        >>> tmp["race"]["track"] = obj.track
        >>> tmp["race"]["cars"] = [{"id": {"color": "red", "name": "random"}, "dimensions": {"length": 1, "width": 2, "guideFlagPosition": 3}}]
        >>> obj.parse_init_data(tmp)
        >>> len(obj.track['pieces'])
        5
        >>> len(obj.track['lanes'])
        3
        >>> len(obj.cars)
        1
        >>> obj.cars[0]["id"]["name"]
        'random'
        """
        race = data.get("race", None)
        if race is None:
            return

        self.track = race.get("track", None)
        if self.track is None:
            return

        cars = race.get("cars", None)
        if cars is None:
            return

        self.cars = cars
        self.parse_car_data(cars)

        session = race.get("raceSession", None)
        if session is not None:
            self.race_laps = session.get("laps", None)
            self.race_duration = session.get("durationMs", -1)
            self.race_max_laptime = session.get("maxLapTimeMs", -1)
            self.race_quickrace = session.get("quickRace", False)
            self.qualifying = self.race_laps is None

    def parse_shortest_path(self, data):
        """ Utilize Dijkstra's algorithm to parse track and find shortests paths

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 100, "angle": 22.5, "switch": True}, {"radius": 100, "angle": 22.5} ]}
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 10 }]
        >>> tmp = {"race": {}}
        >>> tmp["race"]["track"] = obj.track
        >>> obj.parse_shortest_path(tmp)
        >>> len(obj.track_path_tree)
        10
        >>> '0A' in obj.track_path_tree
        True
        >>> '4A' in obj.track_path_tree
        True
        >>> '4B' in obj.track_path_tree
        True
        >>> sorted(obj.track_path_tree['4A'].items()) # doctest: +ELLIPSIS
        [('0A', 43.1...)]
        >>> sorted(obj.track_path_tree['1A'].items())
        [('2A', 120), ('2B', 120)]
        >>> sorted(obj.track_path_tree['3A'].items()) # doctest: +ELLIPSIS
        [('4A', 43.1...), ('4B', 61.19...)]
        >>> obj.track = {u'pieces': [{u'length': 69.0}, {u'length': 100.0}, {u'angle': 45.0, u'switch': True, u'radius': 100}, {u'angle': -22.5, u'radius': 200}, {u'angle': -22.5, u'radius': 100}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'bridge': True, u'length': 67.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 50.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'bridge': True, u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 50.0}, {u'angle': -22.5, u'radius': 100}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}]}
        >>> tmp["race"]["track"] = obj.track
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 10 }]
        >>> obj.parse_shortest_path(tmp)
        >>> len(obj.track_path_tree)
        128
        """

        if 'pieces' not in self.track:
            return

        self.track_path_tree = self.make_track_path_tree(data)
        if self.debug:
            print ("Path tree: %s" % self.track_path_tree)

        num_pieces = len(self.track["pieces"])
        #current_pos_path = '0A'
        end_pos = num_pieces - 1

        #end_pos_path = '%sA' % (end_pos)

        ending = 0
        for piece in self.track["pieces"]:
            if "radius" in piece:
                if piece["angle"] > 0:
                    ending = 1
                break
        number_of_lanes = len(self.track["lanes"])
        self.end_pos_path = '%s%s' % (end_pos, chr(ord('A') + (number_of_lanes * ending - 1)))
        current_pos_path = self.end_pos_path

        self.shortest_path = Dijkstra.shortestPath(self.track_path_tree, current_pos_path, self.end_pos_path)
        if self.debug:
            print ("SHORTPATH: %s" % (self.shortest_path))

    def analyze_track(self):
        """ Does analysis on track

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.analyze_track()
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 100, "angle": 22.5} ]}
        >>> obj.track_sections
        {}
        >>> obj.analyze_track()
        >>> len(obj.track_sections)
        4
        >>> sorted(obj.track_sections[0].items())
        [('crash_cnt', 0), ('end', 1), ('len', 2), ('max_angle', 0), ('ok', False), ('prev_speed', -1), ('speed', -1), ('start', 0), ('type', 'lane')]
        >>> sorted(obj.track_sections[1].items())
        [('index', 0), ('type', 'ref')]
        >>> sorted(obj.track_sections[2].items())
        [('crash_cnt', 0), ('end', 3), ('len', 2), ('max_angle', 0), ('ok', False), ('prev_speed', -1), ('speed', -1), ('start', 2), ('type', 'curve')]
        """
        if 'pieces' not in self.track:
            return
        if self.analyze_done:
            return

        current_section = None
        current_begin = 0
        index = 0
        for piece in self.track["pieces"]:
            if 'radius' in piece and 'angle' in piece:
                next_section_type = 'curve'
            else:
                next_section_type = 'lane'

            if current_section is None:
                current_section = next_section_type
                current_begin = 0
            elif current_section != next_section_type:
                self.track_sections[current_begin] = {'type': current_section, 'len': (index - current_begin), 'end': index - 1, 'start': current_begin, 'speed': -1, 'prev_speed': -1, 'max_angle': 0, 'ok': False, 'crash_cnt': 0}
                current_section = next_section_type
                current_begin = index
            else:
                self.track_sections[index] = {'type': 'ref', 'index': current_begin}
            index += 1

        index -= 1
        if index in self.track_sections:
            if self.track_sections[index]['type'] == current_section:
                self.track_sections[index] = {'type': 'ref', 'index': current_begin}
            else:
                self.track_sections[current_begin] = {'type': current_section, 'len': (index - current_begin + 1), 'end': index, 'start': current_begin, 'speed': -1, 'prev_speed': -1, 'max_angle': 0, 'ok': False, 'crash_cnt': 0}

        self.analyze_done = True

    def get_piece_section(self, index):
        """ Get piece section, for analyzing biggers parts of the track

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 100, "angle": 22.5}, {"length": 100.0} ]}
        >>> obj.analyze_track()
        >>> sorted(obj.get_piece_section(0).items())
        [('crash_cnt', 0), ('end', 1), ('len', 2), ('max_angle', 0), ('ok', False), ('prev_speed', -1), ('speed', -1), ('start', 0), ('type', 'lane')]
        >>> obj.get_piece_section(0) == obj.get_piece_section(1)
        True
        >>> obj.get_piece_section(0) == obj.get_piece_section(2)
        False
        >>> obj.get_piece_section(2) == obj.get_piece_section(3)
        True
        >>> obj.get_piece_section(3) == obj.get_piece_section(4)
        False
        >>> obj.get_piece_section(0) == obj.get_piece_section(4)  # FIXME Works a bit falsely at end of the track
        False
        """
        if index not in self.track_sections:
            return None

        data = self.track_sections[index]
        if data['type'] == 'ref':
            data = self.track_sections[data['index']]

        return data

    def on_game_init(self, data):
        """ We got gameInit message
        """
        # FIXME: We get two inits!!!
        if self.verbose:
            print("INIT: %s" % data)
        self.parse_init_data(data)
        if not self.shortest_path:
            self.parse_shortest_path(data)
        self.analyze_track()

    def get_car_pos(self):
        """ Gets the car position from parsed data

        >>> obj = Ecar42Bot(None, "", "")
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', -1), ('lane', {'startLaneIndex': -1, 'endLaneIndex': -1}), ('lap', -1), ('pieceIndex', -1)]
        >>> obj.car_pos = {"pieceIndex": 5, "inPieceDistance": 20}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', 20), ('lane', {'startLaneIndex': -1, 'endLaneIndex': -1}), ('lap', -1), ('pieceIndex', 5)]
        >>> obj.car_pos = {"pieceIndex": 5, "inPieceDistance": 20, "lane": {"startLaneIndex": 1, "endLaneIndex": 2}}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', 20), ('lane', {'startLaneIndex': 1, 'endLaneIndex': 2}), ('lap', -1), ('pieceIndex', 5)]
        >>> obj.car_pos = {"lane": {"startLaneIndex": 1, "endLaneIndex": 2}}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', -1), ('lane', {'startLaneIndex': 1, 'endLaneIndex': 2}), ('lap', -1), ('pieceIndex', -1)]
        >>> obj.car_pos = {"lane": {"startLaneIndex": 1}}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', -1), ('lane', {'startLaneIndex': 1, 'endLaneIndex': -1}), ('lap', -1), ('pieceIndex', -1)]
        >>> obj.car_pos = {"pieceIndex": 5, "inPieceDistance": 20, "lap": 0, "lane": {"startLaneIndex": 1, "endLaneIndex": 2}}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', 20), ('lane', {'startLaneIndex': 1, 'endLaneIndex': 2}), ('lap', 0), ('pieceIndex', 5)]
        >>> obj.car_pos = {"pieceIndex": 5, "inPieceDistance": 20, "lap": 42}
        >>> sorted(obj.get_car_pos().items())
        [('inPieceDistance', 20), ('lane', {'startLaneIndex': -1, 'endLaneIndex': -1}), ('lap', 42), ('pieceIndex', 5)]
        """
        if "pieceIndex" not in self.car_pos:
            self.car_pos["pieceIndex"] = -1
        if "inPieceDistance" not in self.car_pos:
            self.car_pos["inPieceDistance"] = -1
        if "lap" not in self.car_pos:
            self.car_pos["lap"] = -1
        if "lane" not in self.car_pos:
            self.car_pos["lane"] = {"startLaneIndex": -1, "endLaneIndex": -1}
        else:
            if "startLaneIndex" not in self.car_pos["lane"]:
                self.car_pos["lane"]["startLaneIndex"] = -1
            if "endLaneIndex" not in self.car_pos["lane"]:
                self.car_pos["lane"]["endLaneIndex"] = -1

        return self.car_pos.copy()

    def update_piece_length(self, track_data):
        """ Update track length
        Calculates curve length, this must be utilized after changing
        radius information because of proper lane.

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.track = {"pieces": [{"length": 100}, {"length": 100}, ]}
        >>> sorted(obj.update_piece_length({"angle": 45, "radius": 100}).items()) # doctest: +ELLIPSIS
        [('angle', 45), ('length', 78.5...), ('radius', 100)]
        >>> sorted(obj.update_piece_length({"angle": 45, "radius": 90}).items()) # doctest: +ELLIPSIS
        [('angle', 45), ('length', 70.68...), ('radius', 90)]
        >>> sorted(obj.update_piece_length({"angle": 45, "radius": 110}).items()) # doctest: +ELLIPSIS
        [('angle', 45), ('length', 86.39...), ('radius', 110)]
        >>> sorted(obj.update_piece_length({"angle": 22.5, "radius": 100}).items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 39.2...), ('radius', 100)]
        >>> sorted(obj.update_piece_length({"angle": 22.5, "radius": 200}).items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 78.5...), ('radius', 200)]
        >>> sorted(obj.update_piece_length({"angle": 90, "radius": 200}).items()) # doctest: +ELLIPSIS
        [('angle', 90), ('length', 314.1...), ('radius', 200)]
        """

        if "radius" not in track_data:
            return track_data
        if "angle" not in track_data:
            return track_data

        if track_data["radius"] != 0 and track_data["angle"] != 0:
            track_data["length"] = abs(track_data["radius"]) * abs(track_data["angle"]) * math.pi / 180

        return track_data

    def get_track_data(self, index):
        """ Gets the car track data

        >>> obj = Ecar42Bot(None, "", "")
        >>> sorted(obj.get_track_data(0).items())
        [('angle', 0), ('length', 0.0), ('radius', 0), ('switch', False)]
        >>> obj.track = {"pieces": []}
        >>> sorted(obj.get_track_data(0).items())
        [('angle', 0), ('length', 0.0), ('radius', 0), ('switch', False)]
        >>> obj.track = {"pieces": [{"length": 100}, {"length": 100}, ]}
        >>> sorted(obj.get_track_data(0).items())
        [('angle', 0), ('length', 100), ('radius', 0), ('switch', False)]
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> sorted(obj.get_track_data(0).items())
        [('angle', 0), ('length', 100.0), ('radius', 0), ('switch', False)]
        >>> sorted(obj.get_track_data(1).items())
        [('angle', 0), ('length', 100), ('radius', 0), ('switch', True)]
        >>> sorted(obj.get_track_data(2).items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 78.53...), ('radius', 200), ('switch', False)]
        >>> sorted(obj.get_track_data(3).items())
        [('angle', 0), ('length', 100.0), ('radius', 0), ('switch', False)]
        >>> sorted(obj.get_track_data(None).items())
        [('angle', 0), ('length', 0.0), ('radius', 0), ('switch', False)]
        """
        track_data = {"length": 0.0, "radius": 0, "angle": 0, "switch": False}

        if self.track is None:
            return track_data
        if index is None or index < 0:
            return track_data

        pieces = self.track.get("pieces", [])
        num_pieces = len(pieces)
        if not num_pieces:
            return track_data
        index %= num_pieces

        current_piece = pieces[index]
        track_data.update(current_piece)

        return self.update_piece_length(track_data)

    def get_lane_data(self, current_lane):
        """ Gets the lane data

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.get_lane_data(0) is None
        True
        >>> obj.track["lanes"] = [{"index":1}]
        >>> obj.get_lane_data(0) is None
        True
        >>> obj.track["lanes"] = [{"index":0}]
        >>> obj.get_lane_data(0)
        0
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 0 }, {"index":2, "distanceFromCenter": 10 }]
        >>> obj.get_lane_data(0)
        -10
        >>> obj.get_lane_data(1)
        0
        >>> obj.get_lane_data(2)
        10
        >>> obj.get_lane_data(3) is None
        True
        >>> obj.get_lane_data(-1) is None
        True
        >>> obj.track["lanes"] = [{"index":-1, "distanceFromCenter": 1}]
        >>> obj.get_lane_data(-1) is None
        True
        """
        if "lanes" not in self.track:
            return None
        num_lanes = len(self.track["lanes"])
        if current_lane >= num_lanes or current_lane < 0:
            return None

        for lane in self.track["lanes"]:
            if "index" not in lane:
                continue
            if lane["index"] == current_lane:
                if "distanceFromCenter" not in lane:
                    return 0
                return lane["distanceFromCenter"]

        return None

    def get_target_speed(self, piece_info):
        """ Returns target speed (units/gameTick) for given location

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.get_target_speed({"angle": 45, "radius": 100}) # doctest: +ELLIPSIS
        10.0
        >>> obj.get_target_speed({"angle": 0, "radius": 100})
        20
        >>> obj.get_target_speed(None)
        0
        >>> obj.get_target_speed({})
        0
        >>> obj.get_target_speed(1)
        0
        """
        if piece_info is None or type(piece_info) != dict or "angle" not in piece_info:
            return 0

        if abs(piece_info["angle"]) > 0:
            current_section = None
            if 'pieceIndex' in self.car_pos:
                current_section = self.get_piece_section(self.car_pos['pieceIndex'])

            if current_section is not None and 'speed' in current_section and current_section['speed'] > 0:
                the_speed = current_section['speed']
            else:
                the_speed = self.corner_speed

            # FIXME Did I just hardcode for imola? Yes I did...
            if 'id' in self.track and self.track['id'] == 'imola':
                self.DEC_COUNT_MUL = 20
                the_speed = self.corner_speed

            inner = (the_speed - self.friction_constant) * piece_info["radius"]
            if inner <= 0:
                #return the_speed * piece_info["radius"]
                inner = 0.01

            # Allow only 4 crashes per lap, then limit our speed to a safe value
            if self.crashes_per_lap >= self.CRASHES_PER_LAP_LIMIT:
                inner = min(inner, self.corner_speed * 20)

            return math.sqrt(inner)

        return self.MAX_TARGET_SPEED

    def only_lane_left(self, car_pos):
        """ Determine if there's on curves left, ie. only lanes on this lap

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 100, "angle": 22.5}, {"length": 100.0} ]}
        >>> obj.only_lane_left({"pieceIndex": 0})
        False
        >>> obj.only_lane_left({"pieceIndex": 1})
        False
        >>> obj.only_lane_left({"pieceIndex": 2})
        False
        >>> obj.only_lane_left({"pieceIndex": 3})
        False
        >>> obj.only_lane_left({"pieceIndex": 4})
        True
        >>> obj.only_lane_left({"pieceIndex": 5})
        False
        """
        if not "pieces" in self.track:
            return False
        if not 'pieceIndex' in car_pos:
            return False

        index = car_pos["pieceIndex"]
        num_pieces = len(self.track["pieces"])
        if index >= num_pieces:
            return False

        while index < num_pieces:
            tmp = self.track['pieces'][index]
            if 'radius' in tmp and tmp['radius'] != 0:
                return False
            index += 1

        return True

    def estimate_turbo(self, car_pos, piece_info, next_curve_index):
        """ Estimates if we can use turbo

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 0, "lane": {"startLaneIndex": 0, "endLaneIndex": 1}}
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.speed["speed"] = 6.0
        >>> piece = {"radius": 0}
        >>> obj.curr_tick = 0
        >>> obj.turbo_start = 0
        >>> obj.turbo_end = 30
        >>> obj.turbo_factor = 3
        >>> obj.turbo_on = False
        >>> obj.estimate_turbo(obj.car_pos, piece, 2)
        False
        >>> obj.turbo_end = 10
        >>> obj.turbo_on = False
        >>> obj.estimate_turbo(obj.car_pos, piece, 2)
        False
        >>> obj.turbo_on = False
        >>> obj.turbo_end = 30
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"length": 100}, {"length": 100}, {"length": 100}, {"length": 100}, {"length": 100}]}
        >>> obj.estimate_turbo(obj.car_pos, piece, 5)
        False
        >>> obj.turbo_on = False
        >>> obj.turbo_start = 10
        >>> obj.turbo_end = 30
        >>> obj.curr_tick = 10
        >>> obj.estimate_turbo(obj.car_pos, piece, 6)
        False
        """
        # Disable for others than last lap
        if self.lap != self.race_laps - 1:
            return False

        if self.only_lane_left(car_pos):
            self.enable_turbo()
            return True

        return False

        if self.curr_tick >= self.turbo_start and self.turbo_start > 0 and self.curr_tick <= self.turbo_end and piece_info["radius"] == 0:

            current_pos = self.track_length(car_pos["pieceIndex"], at_end=False) + car_pos["inPieceDistance"]
            length_to_curve = self.track_length(next_curve_index, at_end=False)
            if length_to_curve < current_pos:
                length_to_curve += self.track_length()

            time_of_turbo = (self.turbo_end - self.curr_tick) + 1
            if time_of_turbo <= 0:
                return False

            # Estimate of the turbo length
            #length_of_turbo = time_of_turbo * self.turbo_factor * self.speed["speed"] * self.TURBO_SAFETY_FACTOR

            #length_of_turbo = (time_of_turbo * self.acceleration_initial_turbo - self.slowdown) * self.speed["speed"]
            length_of_turbo = time_of_turbo * self.acceleration_initial_turbo * self.speed["speed"]

            #if self.verbose:
            #    print ("TURBO %s vs. %s, time %s, factor %s" % (length_to_curve - current_pos, length_of_turbo, self.turbo_end - self.curr_tick, self.turbo_factor))
            if length_of_turbo > 0 and (length_to_curve - current_pos) >= length_of_turbo:
                if self.verbose:
                    print ("GOT TURBO, length: %s" % (length_of_turbo))
                self.enable_turbo()
                return True

        return False

    def update_radius_with_lane(self, piece_info, car_pos):
        """ Update piece info radius with lane information

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]
        >>> obj.track["lanes"] = [{"index": 0, "distanceFromCenter": -10}, {"index": 1, "distanceFromCenter": 10 }]
        >>> piece_info = {"radius": 100, "length": 79, "angle": 45}
        >>> obj.update_radius_with_lane(piece_info, obj.car_pos)["radius"]
        110
        >>> obj.car_pos["lane"]["startLaneIndex"] = 1
        >>> piece_info = {"radius": 100, "length": 79, "angle": 45}
        >>> obj.update_radius_with_lane(piece_info, obj.car_pos)["radius"]
        90
        >>> piece_info = {"radius": 100, "length": 79, "angle": -45}
        >>> obj.update_radius_with_lane(piece_info, obj.car_pos)["radius"]
        110
        """
        if not 'radius' in piece_info:
            return piece_info

        # Determine lane
        lane_info_start = self.get_lane_data(car_pos["lane"]["startLaneIndex"])
        lane_info_end = self.get_lane_data(car_pos["lane"]["endLaneIndex"])

        if car_pos["inPieceDistance"] < piece_info["length"] / 2:
            lane_info = lane_info_start
        else:
            lane_info = lane_info_end

        # Adjust our position
        if lane_info is not None and abs(piece_info["radius"]) > 0:
            if piece_info["angle"] > 0:
                piece_info["radius"] -= lane_info
            else:
                piece_info["radius"] += lane_info

            piece_info = self.update_piece_length(piece_info)

        return piece_info

    def length_to_next_switch(self, pos):
        """ Get length to next switch from current pos

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> pos = {"pieceIndex": 0, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.length_to_next_switch(None)
        -1
        >>> obj.length_to_next_switch(0)
        -1
        >>> obj.length_to_next_switch(pos)
        -1
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": -42, "switch": True} ]
        >>> obj.length_to_next_switch(pos)
        80.0
        >>> pos = {"pieceIndex": 0, "inPieceDistance": 98, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.length_to_next_switch(pos)
        2.0
        >>> pos = {"pieceIndex": 1, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.length_to_next_switch(pos) # doctest: +ELLIPSIS
        0
        >>> pos = {"pieceIndex": 1, "inPieceDistance": 42, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.length_to_next_switch(pos) # doctest: +ELLIPSIS
        136.53...
        """
        if type(pos) != dict:
            return -1
        if 'pieceIndex' not in pos:
            return -1
        if 'pieces' not in self.track:
            return -1

        index = pos['pieceIndex']
        piece_info = self.get_track_data(index)
        if 'switch' in piece_info and piece_info['switch'] and pos['inPieceDistance'] < piece_info['length'] / 3:
            return 0

        (switch_index, _) = self.get_next_switch(index)

        length_to_switch = self.difference_between_pieces(index, switch_index)
        length_to_switch -= pos['inPieceDistance']

        return length_to_switch

    def get_next_switch(self, index):
        """ Get information about the next switch

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.get_next_switch(0)
        (-1, None)
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": -42, "switch": True} ]
        >>> sorted(obj.get_next_switch(0)[1].items()) # doctest: +ELLIPSIS
        [('angle', 0), ('length', 100), ('radius', 0), ('switch', True)]
        >>> sorted(obj.get_next_switch(1)[1].items()) # doctest: +ELLIPSIS
        [('angle', -42), ('length', 91.62978572970229), ('radius', 125), ('switch', True)]
        >>> sorted(obj.get_next_switch(2)[1].items()) # doctest: +ELLIPSIS
        [('angle', -42), ('length', 91.62978572970229), ('radius', 125), ('switch', True)]
        >>> sorted(obj.get_next_switch(3)[1].items()) # doctest: +ELLIPSIS
        [('angle', 0), ('length', 100), ('radius', 0), ('switch', True)]
        >>> obj.get_next_switch(20)[0]
        1
        >>> obj.get_next_switch(10)[0]
        3
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]
        >>> obj.get_next_switch(2)[0]
        1
        >>> obj.get_next_switch(3)[0]
        1
        >>> obj.track["pieces"] = [{"radius": 100.0, "angle": 1}, {"radius": 100, "angle": 1, "switch": False}, {"radius": 200, "angle": 22.5} ]
        >>> obj.get_next_switch(0)[0]
        -1
        """
        switch_index = index
        if not "pieces" in self.track:
            return (-1, None)
        num_pieces = len(self.track["pieces"])

        while True:
            switch_index += 1
            switch_index %= num_pieces

            if switch_index == index:
                break

            piece_info = self.get_track_data(switch_index)
            if 'switch' in piece_info and piece_info['switch']:
                return (switch_index, piece_info)

        return (-1, None)

    def difference_between_pieces(self, index, end_index):
        """ Get length till next straight piece

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.car_pos = {"pieceIndex": 2, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 1}}
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.difference_between_pieces(0, 1) # doctest: +ELLIPSIS
        100.0
        >>> obj.difference_between_pieces(0, 2) # doctest: +ELLIPSIS
        200.0
        >>> obj.difference_between_pieces(1, 3) # doctest: +ELLIPSIS
        278.53...
        >>> obj.difference_between_pieces(0, 3) # doctest: +ELLIPSIS
        278.53...
        >>> obj.difference_between_pieces(0, -1) # doctest: +ELLIPSIS
        0.0
        >>> obj.difference_between_pieces(-1, 1) # doctest: +ELLIPSIS
        0.0
        """
        curr_index = index
        length_of_curve = 0
        num_pieces = len(self.track["pieces"])

        if end_index >= num_pieces:
            end_index = num_pieces
        if end_index < 0 or index < 0:
            return 0.0

        while curr_index != end_index:
            piece_info = self.get_track_data(curr_index)
            length_of_curve += piece_info["length"]
            curr_index = (curr_index + 1) % num_pieces
            if curr_index == index:
                break

        return length_of_curve

    def estimate_throttle(self):
        """ Estimates the cars speed at current position

        >>> obj = Ecar42Bot(None, "", "")
        >>> #obj.verbose = False
        >>> obj.slowdown = 1
        >>> obj.car_pos = {"pieceIndex": 5, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 1}}
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.speed["speed"] = 7
        >>> obj.parse_dimensions({"dimensions": {"length": 40.0, "width": 20.0, "guideFlagPosition": 10.0}})
        True
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.car_pos["pieceIndex"] = 0
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.car_pos["pieceIndex"] = 2
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.car_pos["inPieceDistance"] = 90
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.car_pos["pieceIndex"] = 0
        >>> obj.car_angle = 10
        >>> obj.estimate_throttle() # doctest: +ELLIPSIS
        1.0
        >>> obj.track = {"pieces": [{"length": 100.0}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.car_pos["pieceIndex"] = 0
        >>> obj.estimate_throttle()
        1.0
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.car_pos["inPieceDistance"] = 51.5
        >>> obj.estimate_throttle()
        1.0
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.car_pos["inPieceDistance"] = 60.6
        >>> obj.estimate_throttle()
        0.0
        >>> obj.slowdown = 0.1
        >>> obj.car_pos["pieceIndex"] = 0
        >>> obj.estimate_throttle()
        1.0
        """
        car_pos = self.get_car_pos()

        # If last lap and last straight, go for it!
        if self.lap == self.race_laps - 1:
            if self.only_lane_left(car_pos):
                return 1.0

        if "speed" not in self.speed:
            return 0.5

        piece_info = self.get_track_data(car_pos["pieceIndex"])
        (next_curve_index, next_curve_info) = self.get_next_curve(car_pos["pieceIndex"])
        if next_curve_index < car_pos["pieceIndex"]:
            (next_curve_index, next_curve_info) = self.get_next_curve(car_pos["pieceIndex"] + 1)
        if next_curve_info is None:
            return 0.5

        max_throttle = 1.0

        piece_info = self.update_radius_with_lane(piece_info, car_pos)

        if self.estimate_turbo(car_pos, piece_info, next_curve_index):
            return None

        limit_throttle = max_throttle

        car_pos_copy = car_pos.copy()
        car_pos_copy["lane"]["startLaneIndex"] = car_pos["lane"]["endLaneIndex"]
        car_pos_copy["lane"]["endLaneIndex"] = car_pos["lane"]["endLaneIndex"]
        next_curve_info = self.update_radius_with_lane(next_curve_info, car_pos_copy)

        if piece_info["radius"] != 0:
            sect = self.get_piece_section(car_pos["pieceIndex"])
            if sect is not None:
                if abs(self.car_angle) > abs(sect['max_angle']):
                    sect['max_angle'] = abs(self.car_angle)

                # FIXME Hardcoded
                if 'observations' in sect:
                    sect['observations'] += 1
                    sect['lap'] = self.lap
                else:
                    sect['observations'] = 1
                    sect['lap'] = self.lap
                    sect['change_lap'] = self.lap

                if abs(sect['max_angle']) >= 55:
                    # Too much of speed!
                    sect['prev_speed'] = sect['speed']
                    sect['ok'] = True
                elif sect['crash_cnt'] <= 2 and sect['lap'] > sect['change_lap'] and abs(sect['max_angle']) <= 40:
                    #elif sect['crash_cnt'] <= 2 and sect['lap'] > sect['change_lap'] and sect['observations'] > 30 and sect['observations'] % 10 == 0 and abs(sect['max_angle']) <= 40:
                    #elif sect['lap'] > 0 and sect['observations'] > 10 and sect['observations'] % 5 == 0 and not sect['ok'] and abs(sect['max_angle']) <= 45 and (self.prev_change + 5) < self.curr_tick:
                    #elif sect['lap'] > 0 and sect['observations'] % 5 == 0 and not sect['ok'] and abs(sect['max_angle']) <= 45:
                    # We can increase the speed?
                    sect['prev_speed'] = sect['speed']
                    if abs(sect['max_angle']) < 20:
                        sect['speed'] += self.crash_dec_cnt
                    elif abs(sect['max_angle']) < 30:
                        sect['speed'] += self.crash_dec_cnt / 2
                    else:
                        sect['speed'] += self.crash_dec_cnt / 4
                    sect['change_lap'] = self.lap

                    self.prev_change = self.curr_tick
                elif abs(sect['max_angle']) >= 50:
                    sect['ok'] = True
                    sect['prev_speed'] = sect['speed']

        # FIXME Hardcoded
        if abs(self.car_angle) >= 50:
            return 0.0

        target_speed = self.get_target_speed(piece_info)
        next_target_speed = self.get_target_speed(next_curve_info)

        speed_diff = target_speed - self.speed["speed"]
        next_speed_diff = next_target_speed - self.speed["speed"]

        unitchange = speed_diff
        next_unitchange = next_speed_diff
        next_piece_length_diff = next_curve_info["length"] - car_pos["inPieceDistance"]

        if next_curve_index < car_pos["pieceIndex"]:
            curve_index_add = len(self.track["pieces"])
        else:
            curve_index_add = 0

        next_piece_length_diff += (curve_index_add + next_curve_index - car_pos["pieceIndex"]) * next_curve_info["length"]
        safety = self.speed["speed"]
        if self.turbo_on:
            safety *= self.turbo_factor

        if self.curr_tick != self.prev_tick:
            radius_of_momentum = self.car_dimensions["length"] - self.car_dimensions["guideFlagPosition"]
            #torgue_of_car = math.sin(self.car_angle * math.pi / 180) * radius_of_momentum
            inertia = self.car_mass * radius_of_momentum ** 2
            angular_momentum = inertia * (self.car_angle * math.pi / 180 - self.car_angle_prev * math.pi / 180) / (self.curr_tick - self.prev_tick)
            #torgue_of_car == angular_momentum / (curr_tick - prev_tick)
            self.moment_stack.append(angular_momentum / (self.curr_tick - self.prev_tick))
            """
            print ("MOMENT %s %s" % (angular_momentum / (self.curr_tick - self.prev_tick), torgue_of_car))
            print ("FORCE F %s" % (angular_momentum / torgue_of_car))
            """

        ## Oh the magic
        if self.slowdown == 0:
            self.slowdown = 0.0001

        if self.write_log:
            self.log.append({'msgType': 'targetSpeedInfo', 'data': {'targetSpeed': target_speed, 'gameTick': self.curr_tick}})
        if self.debug:
            print ("TARGET SPEED: %s, %s" % (target_speed, next_target_speed))

        target_speed = min(target_speed, next_target_speed)

        # There's again some magic here!
        acceleration_safety = self.acceleration_initial * 0.2

        if unitchange < 0 or abs(unitchange) < acceleration_safety:
            throttle = (target_speed - self.speed["speed"] - self.friction_constant * self.speed["speed"]) * self.acceleration_initial
        else:
            throttle = max_throttle

        # Oh the magic branch!
        if 'id' in self.track and self.track['id'] == 'imola':
            if unitchange < 0 or abs(unitchange) < self.acceleration_initial * self.car_mass * 3:
                throttle = min(throttle, (target_speed - self.speed["speed"] - self.friction_constant * self.speed["speed"]) * self.acceleration_initial)

        next_throttle = (next_target_speed - self.speed["speed"] - self.friction_constant * self.speed["speed"]) * self.acceleration_initial

        # Breaking point detection...
        length_to_slow = (target_speed - self.speed["speed"]) / self.slowdown

        # This seems to be my favourite track, because it needs so many special mentions!
        if 'id' in self.track and self.track['id'] == 'imola':
            length_to_slow *= 1.2

        break_distance = length_to_slow + safety * 2
        if self.debug:
            print ("BREAK DIST: %s, %s, %s, %s" % (break_distance, next_piece_length_diff, next_unitchange, self.speed["speed"]))
        if break_distance >= next_piece_length_diff:
            throttle = next_throttle
        """
        elif break_distance > 0 and next_piece_length_diff > 0 and (next_piece_length_diff - break_distance) < safety * 0.5:  # FIXME TODO
            throttle = min(throttle, next_throttle)
        """

        # We have this theory that Senna's ghost is haunting in Imola,
        # that's why it needs so much of special care...
        if 'id' in self.track and self.track['id'] == 'imola':
            if (next_piece_length_diff - break_distance) < safety * self.car_mass * 3:
                throttle = min(throttle, next_throttle)

        throttle *= limit_throttle
        throttle = clamp(throttle, 0.0, max_throttle)

        if self.debug:
            print ("THROTTLE: %s" % throttle)
        return throttle

    def track_length(self, pos=None, at_end=True, car_pos=None):
        """ Calculates real length of the track

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.track_length() # doctest: +ELLIPSIS
        0.0
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.track_length() # doctest: +ELLIPSIS
        278.5...
        >>> obj.track_length(0) # doctest: +ELLIPSIS
        100.0
        >>> obj.track_length(1) # doctest: +ELLIPSIS
        200.0
        >>> obj.track_length(2) # doctest: +ELLIPSIS
        278.5...
        >>> obj.track_length(3) # overflows, doctest: +ELLIPSIS
        100.0
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": 45} ]}
        >>> obj.track_length() # doctest: +ELLIPSIS
        376.7...
        >>> obj.track = {"pieces": [{"length": 30.0}, {"radius": 70, "angle": 22.5} ]}
        >>> obj.track_length(1) # doctest: +ELLIPSIS
        57.48...
        >>> obj.track_length(1, at_end=False) # doctest: +ELLIPSIS
        30.0
        """
        total = 0.0

        if "pieces" not in self.track:
            return total

        if pos is not None:
            pos %= len(self.track["pieces"])

        for (index, piece) in enumerate(self.track["pieces"]):
            if not at_end and pos is not None and pos == index:
                break
            tmp = piece.copy()
            tmp = self.update_piece_length(tmp)
            if car_pos is not None:
                self.update_radius_with_lane(tmp, car_pos)
            total += tmp["length"]
            if at_end and pos is not None and pos == index:
                break

        return total

    def calculate_speed_on_two(self, speed_prev, speed_curr, tickdiff):
        """ Calculates speed between two pieces

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}]}
        >>> pos1 = {"pieceIndex": 0, "inPieceDistance": 30, "lap": 0, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> pos2 = {"pieceIndex": 0, "inPieceDistance": 45, "lap": 0, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 1).items())
        [('distance_travelled', 15.0), ('speed', 15.0)]
        >>> pos2 = {"pieceIndex": 0, "inPieceDistance": 45, "lap": 1, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 42).items()) # doctest: +ELLIPSIS
        [('distance_travelled', 293.5...), ('speed', 6.9...)]
        >>> pos2 = {"pieceIndex": 1, "inPieceDistance": 0, "lap": 0, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 4).items())
        [('distance_travelled', 70.0), ('speed', 17.5)]
        >>> pos1 = {'pieceIndex': 31, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 0, 'inPieceDistance': 35.24445464789733}
        >>> pos2 = {'pieceIndex': 32, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 0, 'inPieceDistance': 5.66080246460028}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 4).items()) # doctest: +ELLIPSIS
        [('distance_travelled', 70.41...), ('speed', 17.6...)]
        >>> pos1 = {'pieceIndex': 24, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 0, 'inPieceDistance': 80.29866145821346}
        >>> pos2 = {'pieceIndex': 25, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 0, 'inPieceDistance': 4.4863206727347205}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 1).items()) # doctest: +ELLIPSIS
        [('distance_travelled', 24.1...), ('speed', 24.18...)]
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"length": 100.0}, {"length": 90.0}]}
        >>> pos1 = {'pieceIndex': 4, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 0, 'inPieceDistance': 86.728253380267134}
        >>> pos2 = {'pieceIndex': 0, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, 'lap': 1, 'inPieceDistance': 6.728253380267134}
        >>> sorted(obj.calculate_speed_on_two(pos1, pos2, 1).items()) # doctest: +ELLIPSIS
        [('distance_travelled', 10.0), ('speed', 10.0)]
        """
        num_pieces = len(self.track["pieces"])
        prev_index = speed_prev["pieceIndex"]
        index = speed_curr["pieceIndex"]
        lap = speed_curr["lap"]

        if index < 0 or prev_index < 0:
            return {'speed': 0, 'distance_travelled': 0}
        if speed_curr["inPieceDistance"] < 0 or speed_prev["inPieceDistance"] < 0:
            return {'speed': 0, 'distance_travelled': 0}

        if "lap" not in speed_prev:
            return {'speed': 0, 'distance_travelled': 0}
        prev_lap = speed_prev["lap"]

        # Sanity check
        if lap < prev_lap:
            return {'speed': 0, 'distance_travelled': 0}

        if index >= num_pieces:
            lap += 1
        if prev_index >= num_pieces:
            prev_lap += 1

        index %= num_pieces
        prev_index %= num_pieces

        tracklen = self.track_length()

        curr_len = tracklen * lap + self.track_length(index, at_end=False, car_pos=speed_curr) + speed_curr["inPieceDistance"]
        prev_len = tracklen * prev_lap + self.track_length(prev_index, at_end=False, car_pos=speed_prev) + speed_prev["inPieceDistance"]

        tmp = {}
        tmp["distance_travelled"] = (curr_len - prev_len)
        if tmp["distance_travelled"] < 0:
            """
            print ("dist travel", tmp["distance_travelled"], curr_len, prev_len, self.track_length())
            print (self.track_length() * lap, self.track_length(index, at_end=False) , speed_curr["inPieceDistance"], index)
            print (self.track_length() * prev_lap, self.track_length(prev_index, at_end=False), speed_prev["inPieceDistance"], prev_index)
            """
            return {}

        if tickdiff == 0:
            return {}
        tmp["speed"] = float(tmp["distance_travelled"]) / tickdiff
        return tmp

    def calculate_speed(self):
        """ Calculates the speed and other information

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.calculate_speed()
        False
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 20, "lap": 0}
        >>> obj.calculate_speed()
        True
        >>> obj.speed
        {'speed': 0, 'distance_travelled': 0}
        >>> obj.speed = {}
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 30, "lap": 0}
        >>> obj.curr_tick = 2
        >>> obj.prev_tick = 0
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items())
        [('distance_travelled', 10.0), ('distance_travelled_total', 10.0), ('speed', 5.0), ('speed_avg', 5.0)]
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 90, "lap": 0}
        >>> obj.curr_tick = 6
        >>> obj.prev_tick = 0
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 60.0), ('distance_travelled_total', 70.0), ('speed', 10.0), ('speed_avg', 11.6...)]
        >>> obj.car_pos = {"pieceIndex": 3, "inPieceDistance": 10, "lap": 0}
        >>> obj.curr_tick = 10
        >>> obj.prev_tick = 6
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 198.5...), ('distance_travelled_total', 268.5...), ('speed', 49.6...), ('speed_avg', 26.8...)]
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 10, "lap": 0}
        >>> obj.curr_tick = 15
        >>> obj.prev_tick = 10
        >>> obj.calculate_speed()
        False
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 198.5...), ('distance_travelled_total', 268.5...), ('speed', 49.6...), ('speed_avg', 26.8...)]
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 50, "lap": 0}
        >>> obj.curr_tick = 20
        >>> obj.prev_tick = 15
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 40.0), ('distance_travelled_total', 308.5...), ('speed', 8.0), ('speed_avg', 15.42...)]
        >>> obj.car_pos = {"pieceIndex": 3, "inPieceDistance": 99, "lap": 0}
        >>> obj.curr_tick = 25
        >>> obj.prev_tick = 20
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 327.5...), ('distance_travelled_total', 636.07...), ('speed', 65.5...), ('speed_avg', 25.44...)]
        >>> obj.car_pos = {"pieceIndex": 3, "inPieceDistance": 99, "lap": 1}
        >>> obj.curr_tick = 27
        >>> obj.prev_tick = 25
        >>> obj.calculate_speed()
        True
        >>> sorted(obj.speed.items()) # doctest: +ELLIPSIS
        [('distance_travelled', 278.5...), ('distance_travelled_total', 914.6...), ('speed', 139.2...), ('speed_avg', 33.87...)]
        >>> obj.track = {"pieces": [{"length": 30.0}, {"radius": 70, "angle": 22.5} ]}
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 29.989, "lap": 1}
        >>> obj.prev_tick = 1
        >>> obj.curr_tick = 2
        >>> obj.calculate_speed()
        False
        >>> obj.curr_tick = 3
        >>> obj.car_pos = {"pieceIndex": 1, "inPieceDistance": 2, "lap": 1}
        >>> obj.calculate_speed()
        True
        >>> obj.speed['speed'] # doctest: +ELLIPSIS
        1.005...
        >>> obj.car_pos = {"pieceIndex": 1, "inPieceDistance": 27, "lap": 1}
        >>> obj.prev_tick = 2
        >>> obj.curr_tick = 3
        >>> obj.calculate_speed()
        True
        >>> obj.speed['speed'] # doctest: +ELLIPSIS
        25...
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 2, "lap": 2}
        >>> obj.prev_tick = 4
        >>> obj.curr_tick = 5
        >>> obj.calculate_speed()
        True
        >>> obj.speed['speed'] # doctest: +ELLIPSIS
        2.4...
        >>> obj.track = {u'pieces': [{u'length': 69.0}, {u'length': 100.0}, {u'angle': 45.0, u'switch': True, u'radius': 100}, {u'angle': -22.5, u'radius': 200}, {u'angle': -22.5, u'radius': 100}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'bridge': True, u'length': 67.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}, {u'length': 100.0}, {u'angle': 22.5, u'radius': 200}, {u'angle': 22.5, u'radius': 200}, {u'length': 100.0}, {u'length': 50.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'bridge': True, u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 50.0}, {u'angle': -22.5, u'radius': 100}, {u'length': 100.0}, {u'length': 100.0}, {u'length': 100.0, u'switch': True}, {u'angle': 45.0, u'radius': 100}, {u'angle': 45.0, u'radius': 100}, {u'length': 50.0}, {u'angle': -45.0, u'radius': 50}, {u'angle': -45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'angle': 45.0, u'radius': 50}, {u'length': 50.0}, {u'angle': 45.0, u'radius': 50}, {u'angle': 22.5, u'radius': 100}, {u'angle': 22.5, u'radius': 100}, {u'length': 100.0, u'switch': True}]}
        >>> obj.track['lanes'] = [{u'index': 0, u'distanceFromCenter': -10}, {u'index': 1, u'distanceFromCenter': 10}]
        >>> obj.car_pos = {u'pieceIndex': 46, u'lane': {u'startLaneIndex': 1, u'endLaneIndex': 1}, u'lap': 2, u'inPieceDistance': 68.78806374537263}
        >>> obj.prev_tick = 5
        >>> obj.curr_tick = 6
        >>> obj.calculate_speed()
        True
        >>> obj.car_pos = {u'pieceIndex': 47, u'lane': {u'startLaneIndex': 1, u'endLaneIndex': 1}, u'lap': 2, u'inPieceDistance': 5.39756534949025}
        >>> obj.prev_tick = 6
        >>> obj.curr_tick = 7
        >>> obj.calculate_speed()
        True
        >>> obj.speed['speed'] # doctest: +ELLIPSIS
        7.29...
        """
        if "pieces" not in self.track:
            return False

        self.prev_stat = self.curr_stat.copy()
        self.curr_stat = self.get_car_pos().copy()

        # Get the speed stack
        self.speed_stack.append(self.curr_stat.copy())
        self.speed_stack = self.speed_stack[-self.STACK_SIZE:]

        if "pieceIndex" not in self.prev_stat:
            return False

        tickdiff = (self.curr_tick - self.prev_tick)
        tmp = self.calculate_speed_on_two(self.prev_stat, self.curr_stat, tickdiff)

        if not tmp:
            return False

        self.prev_speed = self.speed.copy()
        self.speed.update(tmp)

        if "distance_travelled" in self.speed and self.curr_tick > 0:
            if "distance_travelled_total" not in self.speed:
                self.speed["distance_travelled_total"] = 0
            self.speed["distance_travelled_total"] += self.speed["distance_travelled"]
            self.speed["speed_avg"] = float(self.speed["distance_travelled_total"]) / self.curr_tick

        if len(self.speed_stack) > 0 and len(self.tick_stack) == len(self.speed_stack):
            self.speed_stack_rates = []
            for (ind, item) in enumerate(self.speed_stack):
                if ind == self.STACK_SIZE - 1:
                    break
                tickdiff = (self.curr_tick - self.tick_stack[ind])
                self.speed_stack_rates.append(self.calculate_speed_on_two(item, self.curr_stat, tickdiff))

        if self.verbose:
            print("SPEED: %s" % self.speed)
        #if self.debug:
        #    print("SPEEDSTACK: %s" % self.speed_stack_rates)
        return True

    def calculate_acceleration(self):
        """ Calculate acceleration of a car
        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.calculate_acceleration()
        False
        >>> obj.speed["speed"] = 5.0
        >>> obj.calculate_acceleration()
        False
        >>> obj.prev_speed["speed"] = 0.0
        >>> obj.calculate_acceleration()
        False
        >>> obj.curr_tick = 1
        >>> obj.prev_tick = 0
        >>> obj.calculate_acceleration()
        True
        >>> obj.acceleration
        5.0
        """
        if "speed" not in self.speed or not "speed" in self.prev_speed:
            return False
        if self.curr_tick == self.prev_tick:
            return False

        self.acceleration = (self.speed["speed"] - self.prev_speed["speed"]) / (self.curr_tick - self.prev_tick)
        if self.debug:
            print ("ACCELERATION: %s" % (self.acceleration))

        if self.acceleration_initial == 0:
            self.acceleration_initial = self.acceleration
            if self.verbose:
                print ("INITIAL ACCELERATION: %s, tick %s" % (self.acceleration_initial, self.curr_tick))
        elif self.friction_constant == 0 and self.prev_speed["speed"] != 0 and self.acceleration_initial != 0:
            #self.friction_constant = self.car_mass * self.acceleration_initial - FORCE
            self.friction_constant = self.acceleration_initial / self.GRAVITY
            #self.friction_constant2 = self.acceleration_initial - self.acceleration
            #self.friction_constant2 = (self.speed["speed"] - self.prev_speed["speed"]) / self.acceleration_initial
            #self.friction_constant3 = (2 * self.prev_speed["speed"] - self.speed["speed"]) / self.prev_speed["speed"] ** 2

            # In fact friction constant means slow down (= total of air resistance, friction, etc.) when accelerating
            print ("FRICT: %s" % (self.friction_constant))
            self.learning_slowdown = True
        elif self.learning and self.learning_slowdown:
            piece_diff = self.car_pos["inPieceDistance"] - self.car_pos_prev["inPieceDistance"]
            if piece_diff != 0:
                self.slowdown = (self.speed["speed"] - self.prev_speed["speed"]) / piece_diff

            # This is the slow down when not accelerating
            if self.debug:
                print ("SLOWDOWN: %s" % (self.slowdown))
            if self.slowdown < 0:
                self.learning_slowdown = False

        return True

    def calculate_mass(self):
        """ Calculate mass of a car
        Does not take friction into account

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.acceleration = 1.0
        >>> obj.throttle_amount = 1.0
        >>> obj.calculate_mass()
        True
        >>> obj.acceleration
        1.0
        """
        if self.acceleration == 0:
            return False
        if self.throttle_amount == 0:
            return False
        #Calculate only once on initial acceleration
        if not self.car_mass == 0:
            return True

        #self.car_mass = self.throttle_amount / self.acceleration_initial
        if self.acceleration_initial != 0:
            self.car_mass = 1.0 / self.acceleration_initial
        else:
            self.car_mass = 0.0
        if self.verbose:
            print ("MASS %s" % (self.car_mass))
        return True

    def calculate_air_resistance(self):
        """ Calculate constant for air resistance

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.acceleration = 0.126
        >>> obj.throttle_amount = 1.0
        >>> obj.car_mass = 5.0
        >>> obj.speed["speed"] = 0.1
        >>> obj.calculate_air_resistance()
        True
        >>> obj.air_resistance_constant
        36.99999999999999
        """
        if self.acceleration == 0:
            return False
        if self.throttle_amount == 0:
            return False
        if self.car_mass == 0:
            return False
        if "speed" not in self.speed:
            return False
        if self.speed["speed"] == 0:
            return False

        if not self.air_resistance_constant == 0:
            return True

        if self.verbose:
            print ("Mass %s Throttle %s Acceleration %s Speed %s" % (self.car_mass, self.throttle_amount, self.acceleration, self.speed["speed"]))

        self.air_resistance_constant = (self.throttle_amount - self.car_mass * self.acceleration) / self.speed["speed"] ** 2

        if self.verbose:
            print ("AIR RESISTANCE %s" % (self.air_resistance_constant))

        return True

    def get_next_curve(self, index):
        """ Get information about the next curve

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.get_next_curve(0)
        (-1, None)
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": -42} ]
        >>> sorted(obj.get_next_curve(0)[1].items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 78.5...), ('radius', 200), ('switch', False)]
        >>> sorted(obj.get_next_curve(1)[1].items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 78.5...), ('radius', 200), ('switch', False)]
        >>> sorted(obj.get_next_curve(2)[1].items()) # doctest: +ELLIPSIS
        [('angle', -42), ('length', 91.6...), ('radius', 125), ('switch', False)]
        >>> sorted(obj.get_next_curve(3)[1].items()) # doctest: +ELLIPSIS
        [('angle', 22.5), ('length', 78.5...), ('radius', 200), ('switch', False)]
        >>> obj.get_next_curve(20)[0]
        2
        >>> obj.get_next_curve(10)[0]
        3
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]
        >>> obj.get_next_curve(2)[0]
        -1
        """
        curve_index = index
        if "pieces" not in self.track:
            return (-1, None)
        num_pieces = len(self.track["pieces"])

        while True:
            curve_index += 1
            curve_index %= num_pieces

            if curve_index == index:
                break

            piece_info = self.get_track_data(curve_index)
            if abs(piece_info["angle"]) > 0:
                return (curve_index, piece_info)

        return (-1, None)

    def get_next_switch_index(self, index):
        """ Get information about the next curve

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.get_next_switch_index(0) is None
        True
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": -42}, {"length": 100, "switch": True} ]
        >>> obj.get_next_switch_index(0)
        1
        >>> obj.get_next_switch_index(1)
        4
        >>> obj.get_next_switch_index(2)
        4
        >>> obj.get_next_switch_index(3)
        4
        >>> obj.get_next_switch_index(4)
        1
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5}, {"radius": 125, "angle": -42} ]
        >>> obj.get_next_switch_index(1)
        1
        """
        piece_index = index
        if "pieces" not in self.track:
            return None
        num_pieces = len(self.track["pieces"])

        while True:
            piece_index += 1
            piece_index %= num_pieces

            piece_info = self.get_track_data(piece_index)
            if piece_info["switch"]:
                return piece_index
            if piece_index == index:
                break

        return None

    def calculate_car_position(self, car):
        """ Calculates race positions of one car

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.calculate_car_position(0)
        0.0
        >>> obj.calculate_car_position(None)
        0.0
        >>> obj.calculate_car_position({"pieceIndex": 0, "inPieceDistance": 20, "lap": 0})
        20.0
        >>> obj.calculate_car_position({"pieceIndex": 1, "inPieceDistance": 20, "lap": 0})
        120.0
        >>> obj.calculate_car_position({"pieceIndex": 1, "inPieceDistance": 20, "lap": 1}) # doctest: +ELLIPSIS
        398.5...
        """
        if type(car) != dict:
            return 0.0
        if "lap" not in car or not "pieceIndex" in car or not "inPieceDistance" in car:
            return 0.0

        if car["lap"] < 0:
            return 0.0

        return (self.track_length() * car["lap"]) + self.track_length(car["pieceIndex"], at_end=False) + car["inPieceDistance"]

    def calculate_car_positions(self):
        """ Calculates race positions of all cars

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.car_color = "red"
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 2, "lane": {"startLaneIndex": 1, "endLaneIndex": 1}, "lap": 0}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 0, "inPieceDistance": 20, "lap": 0}, "angle": 124}])
        True
        >>> obj.calculate_car_positions()
        [{102.0: ('blue', 1)}, {20.0: ('red', 0)}]
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 2, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, "lap": 0}, "angle": 44}, {"id": {"color": "red", 'lane': {'startLaneIndex': 1, 'endLaneIndex': 0}, "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, "lap": 0}, "angle": 124}])
        True
        >>> obj.calculate_car_positions()
        [{120.0: ('red', 0)}, {102.0: ('blue', 1)}]
        >>> posdata =obj.calculate_car_positions()
        >>> posdata[0].keys()[0]
        120.0
        >>> posdata[0].values()[0][0]
        'red'
        >>> posdata[0].values()[0][1]
        0
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 2, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},  "lap": 1}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, "lap": 0}, "angle": 124}])
        True
        >>> obj.calculate_car_positions() # doctest: +ELLIPSIS
        [{380.5...: ('blue', 0)}, {120.0: ('red', 0)}]
        """
        cardata = []

        self.car_racepos = self.calculate_car_position(self.car_pos)
        if "lane" in self.car_pos:
            thelane = self.car_pos["lane"]["endLaneIndex"]
        else:
            thelane = 0

        cardata.append({self.car_racepos: (self.car_color, thelane)})

        for color in self.opponents:
            car = self.opponents[color]
            self.opponents[color]["racepos"] = self.calculate_car_position(car["pos"])
            if "lane" in car["pos"] and "endLaneIndex" in car["pos"]["lane"]:
                thelane = car["pos"]["lane"]["endLaneIndex"]
            else:
                thelane = 0
            cardata.append({self.opponents[color]["racepos"]: (color, thelane)})

        return sorted(cardata, reverse=True)

    def determine_free_lane(self, mode="default"):
        """ Determines if there's a free lane available.
        @param mode Mode to determine free lane, "default" checks only reachable cars, "all" checks for all cars, "front" checks all cars before player

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.car_color = "red"
        >>> obj.track = {"pieces": [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]}
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 0 }]
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 2, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1},  "lap": 1}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, 'lane': {'startLaneIndex': 1, 'endLaneIndex': 1}, "lap": 0}, "angle": 124}])
        True
        >>> obj.speed["speed"] = 6
        >>> obj.determine_free_lane("all")
        0
        >>> obj.determine_free_lane("front")
        0
        >>> obj.determine_free_lane() # Other car is too long away to reach
        -1
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 2, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},  "lap": 1}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0}, "lap": 0}, "angle": 124}])
        True
        >>> obj.determine_free_lane() # We are too long away
        -1
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 22, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},  "lap": 0}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0}, "lap": 0}, "angle": 124}])
        True
        >>> obj.determine_free_lane()
        1
        >>> obj.parse_car_data([{"id": {"color": "blue", "name": "random"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 22, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0},  "lap": 0}, "angle": 44}, {"id": {"color": "red", "name": "mycar"}, "piecePosition": {"pieceIndex": 1, "inPieceDistance": 20, 'lane': {'startLaneIndex': 0, 'endLaneIndex': 0}, "lap": 1}, "angle": 124}])
        True
        >>> obj.determine_free_lane() # We are leading
        1
        """
        if "speed" not in self.speed:
            return -1

        nowdata = self.calculate_car_positions()
        if not nowdata:
            return -1

        track_len = self.track_length()
        used_lanes = {}
        for i, data in enumerate(self.track["lanes"]):
            used_lanes[i] = 0

        if mode == "all":
            # Calculate total lane usage
            for car in nowdata:
                lane = car.values()[0][1]
                used_lanes[lane] += 1
        elif mode == "front":
            # Calculate only those before us
            for car in nowdata:
                if car.values()[0][0] == self.car_color:
                    break
                lane = car.values()[0][1]
                used_lanes[lane] += 1
        else:
            got_just = False
            # Calculate those in good distance
            for car in nowdata:
                if car.values()[0][0] == self.car_color:
                    continue

                dist = car.keys()[0]
                catch_dist = self.speed["speed"] * self.REACH_DISTANCE
                switch_dist = self.length_to_next_switch(self.car_pos)

                if (switch_dist < catch_dist and switch_dist >= 0 and
                   (dist > self.car_racepos and (dist - catch_dist) <= self.car_racepos) or
                   ((dist % track_len) > (self.car_racepos % track_len) and
                   ((dist - catch_dist) % track_len) <= (self.car_racepos % track_len))):
                    lane = car.values()[0][1]
                    used_lanes[lane] += 1
                    got_just = True
                #if (dist > self.car_racepos and (dist - catch_dist) <= self.car_racepos) or ((dist % track_len) > (self.car_racepos % track_len) and ((dist - catch_dist) % track_len) <= (self.car_racepos % track_len)):

            if not got_just:
                return -1

        free_lanes = sorted(used_lanes.iteritems(), key=operator.itemgetter(1))

        if self.debug:
            print ("LANES: %s" % (free_lanes))

        return free_lanes[0][0]

    def determine_lane(self):
        """ Determines on which lane to drive on

        >>> obj = Ecar42Bot(None, "", "")
        >>> obj.verbose = False
        >>> obj.determine_lane()
        False
        >>> obj.car_pos = {"pieceIndex": 0, "inPieceDistance": 20, "lane": {"startLaneIndex": 0, "endLaneIndex": 0}}
        >>> obj.track = {}
        >>> obj.track["pieces"] = [{"length": 100.0}, {"length": 100, "switch": True}, {"radius": 200, "angle": 22.5} ]
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 10 }]
        >>> obj.switch_lane = lambda data: print("RES: Switching to %s" % (data))
        >>> obj.determine_lane()
        RES: Switching to Right
        True
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.last_switch = -1
        >>> obj.determine_lane()
        RES: Switching to Right
        True
        >>> obj.determine_lane()
        False
        >>> obj.last_switch = -1
        >>> obj.car_pos["lane"]["startLaneIndex"] = 1
        >>> obj.car_pos["lane"]["endLaneIndex"] = 1
        >>> obj.determine_lane()
        False
        >>> obj.car_pos["pieceIndex"] = 1
        >>> obj.car_pos["inPieceDistance"] = 5
        >>> obj.car_pos["lane"]["endLaneIndex"] = 1
        >>> obj.track["pieces"][2]["angle"] = -22
        >>> obj.determine_lane()
        RES: Switching to Left
        True
        >>> obj.last_switch = -1
        >>> obj.track["pieces"][2]["angle"] = 100
        >>> obj.determine_lane()
        False
        >>> obj.track["lanes"] = [{"index":0, "distanceFromCenter": -10}, {"index":1, "distanceFromCenter": 0 }, {"index":2, "distanceFromCenter": 10 }]
        >>> obj.determine_lane()
        RES: Switching to Right
        True
        >>> obj.car_pos["lane"]["endLaneIndex"] = 1
        >>> obj.last_switch = -1
        >>> obj.determine_lane()
        RES: Switching to Right
        True
        >>> obj.last_switch = -1
        >>> obj.car_pos["lane"]["startLaneIndex"] = 1
        >>> obj.determine_lane()
        RES: Switching to Right
        True
        >>> obj.last_switch = -1
        >>> obj.car_pos["lane"]["endLaneIndex"] = 2
        >>> obj.determine_lane()
        False
        """
        car_pos = self.get_car_pos()
        if self.last_switch == car_pos["pieceIndex"]:
            return False
        piece_info = self.get_track_data(car_pos["pieceIndex"])

        if "lanes" not in self.track:
            return False

        num_lanes = len(self.track["lanes"])
        if num_lanes <= 1:
            return False

        next_switch_index = self.get_next_switch_index(car_pos["pieceIndex"])
        if not piece_info["switch"] and next_switch_index - car_pos["pieceIndex"] > 1:
            return False

        #next_switch_index = self.get_next_switch_index(car_pos["pieceIndex"])
        #if next_switch_index is None:
        #    return False

        #if piece_info["switch"]:
        #    return False

        (_, next_curve) = self.get_next_curve(next_switch_index)
        if next_curve is None:
            return False

        direction = "Stay"

        #num_pieces = len(self.track["pieces"])
        current_pos_path = '%s%s' % (car_pos["pieceIndex"], chr(ord('A') + car_pos["lane"]["endLaneIndex"]))
        #if car_pos["pieceIndex"] > 0:
        #    end_pos = car_pos["pieceIndex"] - 1
        #else:
        #    end_pos = num_pieces - 1

        #end_pos_path = '%s%s' % (end_pos, chr(ord('A') + car_pos["lane"]["startLaneIndex"]))
        if self.track_path_tree:
            self.shortest_path = Dijkstra.shortestPath(self.track_path_tree, current_pos_path, self.end_pos_path)

        if self.shortest_path:
            first_point = None
            second_point = None
            for point in self.shortest_path:
                num_point = int(point[0:-1])
                if first_point is not None:
                    second_point = point
                    break
                if num_point == next_switch_index:
                    first_point = point

            if first_point is None:
                return False
            if second_point is None:
                second_point = first_point
            preferred_lane = ord(second_point[-1]) - ord('A')
            if preferred_lane < car_pos["lane"]["startLaneIndex"]:
                direction = "Left"
            elif preferred_lane > car_pos["lane"]["startLaneIndex"]:
                direction = "Right"
            else:
                direction = "Stay"

            if direction != "Stay" and self.verbose:
                print ("SWITCH AT: %s, %s, %s, %s" % (first_point, preferred_lane, car_pos["lane"]["endLaneIndex"], direction))
        else:
            # Always select inner curve
            if next_curve["angle"] < 0:
                direction = "Left"
            elif next_curve["angle"] > 0:
                direction = "Right"

        freelane = self.determine_free_lane()
        if self.debug:
            print("FREE LANE: %s, %s" % (freelane, car_pos["lane"]["endLaneIndex"]))

        if freelane >= 0:
            if freelane == car_pos["lane"]["endLaneIndex"]:
                return False
            if freelane < car_pos["lane"]["endLaneIndex"]:
                direction = "Left"
            else:
                direction = "Right"

        if direction == "Stay":
            return False

        if car_pos["lane"]["endLaneIndex"] == 0 and direction == "Left":
            return False
        if car_pos["lane"]["endLaneIndex"] == (num_lanes - 1) and direction == "Right":
            return False

        if self.verbose:
            print("SWITCH to %s, %s %s" % (direction, car_pos["lane"]["startLaneIndex"], car_pos["lane"]["endLaneIndex"]))

        self.last_switch = car_pos["pieceIndex"]
        self.switch_lane(direction)

        return True

    def do_learn(self):
        """ The learning phase
        """

        car_pos = self.get_car_pos()
        piece_info = self.get_track_data(car_pos["pieceIndex"])
        if 'radius' not in piece_info or piece_info["radius"] == 0:
            return
        if self.car_angle == 0:
            return
        if 'speed' not in self.speed:
            return

        piece_info = self.update_radius_with_lane(piece_info, car_pos)

        speed_corner = self.speed["speed"] / piece_info["radius"]
        speed_minus_angle = speed_corner - self.car_angle * math.pi / 180
        speed_at_slip = speed_minus_angle * piece_info["radius"]

        self.centrifugal = speed_at_slip ** 2 / piece_info["radius"]

        radius_of_momentum = self.car_dimensions["length"] - self.car_dimensions["guideFlagPosition"]
        torgue_of_car = math.sin(self.car_angle * math.pi / 180) * radius_of_momentum
        inertia = self.car_mass * radius_of_momentum ** 2
        angular_velocity = (self.car_angle * math.pi / 180 - self.car_angle_prev * math.pi / 180) / (self.curr_tick - self.prev_tick)
        angular_momentum = inertia * angular_velocity
        if self.verbose:
            print ("FIRST MOMENT %s %s" % (angular_momentum / (self.curr_tick - self.prev_tick), torgue_of_car))
            print ("FORCE F %s" % (angular_momentum / torgue_of_car))
            print ("MOMENTO %s" % ((self.car_mass * radius_of_momentum ** 2 * angular_velocity) / (radius_of_momentum * math.sin(self.car_angle * math.pi / 180))))

        # Hitausmomentti / Inertia
        # J = car.mass * r ** 2
        # Kulmanopeus (in rad) / angular velocity
        # w = (angle - angle_prev) / (curr_tick - prev_tick)
        # Pyorimismaara / angular momentum
        # L = J * w
        # Vaantomomentti / torgue
        # M = F * radius * sin(angle)
        # angular_momentum / (curr_tick - prev_tick) == torgue
        # L / (curr_tick - prev_tick) = M
        # M / (radius * sin(angle)) = F

        #self.centrifugal2 = self.speed["speed"] ** 2 / piece_info["radius"]
        #print ("centrifugal2: %s" % (self.centrifugal2))
        if not self.centrifugal_stack:
            self.corner_speed = self.centrifugal
            self.crash_dec_cnt = self.centrifugal / self.DEC_COUNT_MUL
            for sect in self.track_sections:
                self.track_sections[sect]['speed'] = self.centrifugal
            print ("centrifugal: %s" % (self.centrifugal))

        self.centrifugal_stack.append(self.centrifugal)
        if self.verbose:
            print ("centrifugal: %s" % (self.centrifugal))
        self.learning = False
        if self.verbose:
            print ("LEARNING DONE")

    def visualize(self):
        if 'name' in self.track and not self.name_printed:
            self.name_printed = True
            print ("TRACK: %s" % (self.track['name']))

        if self.curr_tick % 5 != 0:
            return

        if self.crashed and self.crash_shown and not self.opponents:
            return
        self.crash_shown = False

        if 'pieces' in self.track:
            lendata = len(self.track['pieces'])
            ownpos = 0
            if 'pieceIndex' in self.car_pos:
                ownpos = self.car_pos['pieceIndex']

            op_info = {}
            for car in self.opponents:
                if 'pos' in self.opponents[car] and 'pieceIndex' in self.opponents[car]['pos']:
                    pos = self.opponents[car]['pos']['pieceIndex']
                    if pos not in op_info:
                        op_info[pos] = car[0]
                    else:
                        op_info[pos] = '@'

            if self.car_color is not None and self.car_color:
                own_letter = self.car_color[0].upper()
            else:
                own_letter = 'X'
            pos_str = ''
            for daa in xrange(lendata + 1):
                pos = 0
                if ownpos == daa:
                    pos = 1
                if daa in op_info.keys():
                    pos += 2

                if pos == 1:
                    if self.crashed:
                        self.crash_shown = True
                        pos_str += '!'
                    else:
                        pos_str += own_letter.upper()
                elif pos > 0 and pos % 2 == 0:
                    pos_str += op_info[daa]
                elif pos % 2 == 1:
                    if self.crashed:
                        self.crash_shown = True
                        pos_str += '!'
                    else:
                        pos_str += own_letter.upper()
                else:
                    pos_str += '-'

            if 'speed' in self.speed:
                speed = self.speed['speed']
            else:
                speed = 0.0
            print ('[%s] lap %s, speed %s, lap time: %s    ' % (pos_str, self.lap, speed, self.lap_time), end='\r')
            sys.stdout.flush()

    def on_car_positions(self, data):
        if self.verbose:
            print ("POS: %s" % data)
        self.parse_car_data(data)
        self.calculate_speed()
        self.calculate_acceleration()
        self.calculate_mass()
        self.calculate_air_resistance()

        if self.learning:
            self.do_learn()

            if self.learning_slowdown:
                self.throttle(0.0)
            else:
                self.throttle(self.learning_throttle)
        elif not self.crashed and not self.determine_lane() and not self.turbo_on:
            thro = self.estimate_throttle()
            if thro is not None and thro >= 0.0 and thro <= 1.0:
                self.throttle(thro)

        if self.visual:
            self.visualize()

    def make_track_path_tree(self, data):
        """ Makes the graph for Dijkstra

        """
        graph_tree = {}
        lane_names = ('A', 'B', 'C', 'D')  # Lane index: 0,1,2,3
        lane_radius_variables = self.get_lane_radius_variables(data)

        count_of_lanes = len(data["race"]["track"]["lanes"])
        count_of_pieces = len(data["race"]["track"]["pieces"])
        piece_index = 0
        for piece in data["race"]["track"]["pieces"]:
            next_piece_index = (piece_index + 1) % count_of_pieces

            lane_index = 0
            while lane_index < count_of_lanes:
                paths_for_lane = {}
                next_node_name = str(next_piece_index) + lane_names[lane_index]
                if "switch" in piece:
                    if "length" in piece:
                        path_lenght = piece["length"] + 20
                        self.create_new_path(paths_for_lane, next_node_name, path_lenght)

                        #switch to left, to right
                        # left
                        if lane_index - 1 >= 0:
                            side_node_name = str(next_piece_index) + lane_names[lane_index - 1]
                            path_lenght = piece["length"] + 20
                            self.create_new_path(paths_for_lane, side_node_name, path_lenght)

                        #right
                        if lane_index + 1 < count_of_lanes:
                            side_node_name = str(next_piece_index) + lane_names[lane_index + 1]
                            path_lenght = piece["length"] + 20
                            self.create_new_path(paths_for_lane, side_node_name, path_lenght)

                    if "radius" in piece:
                        # right turn 1, left turn -1
                        turn_direction_indicator = self.get_turn_indicator(piece)
                        path_lenght = self.get_curve_lenght(piece, lane_radius_variables[lane_index], turn_direction_indicator)
                        self.create_new_path(paths_for_lane, next_node_name, path_lenght)
                        #switch to left, to right
                        # left
                        if lane_index - 1 >= 0:
                            travel_correction_term = 1
                            if turn_direction_indicator == 1:
                                travel_correction_term = 18

                            if piece['angle'] > 0:
                                travel_correction_term -= piece['angle'] / 2

                            side_node_name = str(next_piece_index) + lane_names[lane_index - 1]
                            swithing_path_lenght = turn_direction_indicator * travel_correction_term + path_lenght
                            self.create_new_path(paths_for_lane, side_node_name, swithing_path_lenght)

                        # right
                        if lane_index + 1 < count_of_lanes:
                            travel_correction_term = 1
                            if turn_direction_indicator == -1:
                                travel_correction_term = 18

                            if piece['angle'] < 0:
                                travel_correction_term += piece['angle'] / 2

                            side_node_name = str(next_piece_index) + lane_names[lane_index + 1]
                            swithing_path_lenght = turn_direction_indicator * (-1 * travel_correction_term) + path_lenght
                            self.create_new_path(paths_for_lane, side_node_name, swithing_path_lenght)

                elif "length" in piece:
                    self.create_new_path(paths_for_lane, next_node_name, piece["length"])

                elif "radius" in piece:
                    turn_direction_indicator = self.get_turn_indicator(piece)
                    path_lenght = self.get_curve_lenght(piece, lane_radius_variables[lane_index], turn_direction_indicator)
                    self.create_new_path(paths_for_lane, next_node_name, path_lenght)

                current_node_name = str(piece_index) + lane_names[lane_index]
                tmp_dict = {current_node_name: paths_for_lane.copy()}

                graph_tree.update(tmp_dict)
                paths_for_lane.clear()
                lane_index = lane_index + 1
            piece_index = next_piece_index

        return graph_tree

    def get_path_switching_points(self, travel_path):
        lane_name = ""
        prev_lane_name = ""
        switching_points = []
        lane_index = 0
        for path_item in travel_path:
            lane_name = re.sub(r'^[0-9]+', "", path_item)
            if lane_index == 0:
                prev_lane_name = re.sub(r'^[0-9]+', "", path_item)
            else:
                if lane_name != prev_lane_name:
                    switching_points.append(lane_index - 1)
                prev_lane_name = lane_name
            lane_index = lane_index + 1
        return switching_points

    def get_lane_radius_variables(self, data):
        lane_radius_variables = {}
        if 'race' in data and 'track' in data['race'] and 'lanes' in data['race']['track']:
            for index, lane in enumerate(data["race"]["track"]["lanes"]):
                tmp_dict = {index: lane["distanceFromCenter"]}
                lane_radius_variables.update(tmp_dict)
        return lane_radius_variables

    def get_curve_lenght(self, piece, lane_radius_variable, turn_direction_indicator):
        path_lenght = (piece["radius"] + (turn_direction_indicator * lane_radius_variable)) * abs(piece["angle"]) * math.pi / 180
        return path_lenght

    def create_new_path(self, paths_for_lane, side_node_name, path_lenght):
        tmp_dict = {side_node_name: path_lenght}
        paths_for_lane.update(tmp_dict)

    def get_turn_indicator(self, piece):
        turn_direction_indicator = 1
        if 0 < piece["angle"]:
            turn_direction_indicator = -1
        return turn_direction_indicator

    def decrease_corner_speed(self, amount, is_crash=False):
        self.corner_change_time = self.curr_tick

        current_section = None
        if 'pieceIndex' in self.car_pos:
            current_section = self.get_piece_section(self.car_pos['pieceIndex'])
            if self.verbose:
                print ("ORIGIN  SECTION: %s" % (current_section))

        old_speed = self.corner_speed
        if current_section is not None:
            if 'speed' in current_section and current_section['speed'] > 0:
                old_speed = current_section['speed']
            elif 'pieceIndex' in self.car_pos and self.car_pos['pieceIndex'] >= 2:
                prev_section = self.get_piece_section(self.car_pos['pieceIndex'] - 2)
                if 'speed' in prev_section and prev_section['speed'] > 0:
                    old_speed = prev_section['speed']

        if amount > old_speed:
            new_speed = old_speed * 0.9
        else:
            new_speed = old_speed - amount

        if self.centrifugal_stack and new_speed < self.centrifugal_stack[0] * self.SAFE_LOW:
            new_speed = self.centrifugal_stack[0] * self.SAFE_LOW

        if new_speed < 0:
            new_speed = 0.01

        if current_section is not None:
            if is_crash:
                current_section['crash_cnt'] += 1
            current_section['ok'] = True
            current_section['prev_speed'] = current_section['speed']
            current_section['speed'] = new_speed
            if self.verbose:
                print ("UPDATED SECTION: %s" % (current_section))

        ind = self.car_pos['pieceIndex']
        if ind >= 3:
            index_prev = ind - 2
            while index_prev < ind:
                prev_section = self.get_piece_section(index_prev)
                prev_section['prev_speed'] = prev_section['speed']

                dec_cnt = prev_section['speed'] / 10  # Update prev as well
                prev_section['speed'] -= dec_cnt
                index_prev += 1
        """
        """
        self.corner_speed = min(new_speed, self.corner_speed)

    def on_crash(self, data):
        if "color" in data:
            if data["color"] == self.car_color:
                self.crash_count += 1
                self.crashes_per_lap += 1
                self.turbo_start = -1
                self.turbo_end = -1
                self.turbo_on = False
                self.crashed = True
                #self.decrease_corner_speed(self.corner_speed / 2)  # FIXME Figure out best ratio
                if not self.learning:
                    if self.crash_dec_cnt:
                        ##self.decrease_corner_speed(self.crash_dec_cnt * self.DEC_COUNT_MUL, is_crash=True)
                        self.decrease_corner_speed(self.crash_dec_cnt / 1.5, is_crash=True)  # FIXME Hardcoded factor
                    else:
                        self.decrease_corner_speed(self.corner_speed / 3, is_crash=True)  # FIXME Figure out best ratio
                if self.verbose:
                    print("we crashed, corner: %s" % (self.corner_speed))
                    #print ("MOMENT STACK: %s" % (self.moment_stack[-15:]))
            elif self.verbose:
                print("%s crashed" % (data["color"]))

    def on_spawn(self, data):
        if "color" in data:
            if data["color"] == self.car_color:
                self.crashed = False
            elif self.verbose:
                print("%s spawned" % (data["color"]))

    def on_turbo(self, data):
        if "turboDurationTicks" not in data:
            return

        self.turbo_start = self.curr_tick
        self.turbo_end = self.turbo_start + data["turboDurationTicks"]

        if "turboFactor" in data:
            self.turbo_factor = data["turboFactor"]
            self.acceleration_initial_turbo = self.acceleration_initial * self.turbo_factor
        else:
            self.turbo_factor = -1
            self.acceleration_initial_turbo = self.acceleration_initial * 4.2  # Just some random value

        if self.verbose:
            print("TURBO is on from %s to %s, factor %s" % (self.turbo_start, self.turbo_end, self.turbo_factor))

    def on_turbo_start(self, data):
        print("TURBO start")
        self.turbo_on = True

    def on_turbo_end(self, data):
        print("TURBO end")
        self.turbo_on = False

    def on_game_end(self, data):
        print("Race ended")
        self.crashed = False

    def on_race_session(self, data):
        self.speed['speed'] = 0
        self.prev_speed = {'speed': 0}
        self.crashed = False

    def on_lap_finished(self, data):
        if self.verbose:
            print("LAP: %s" % (data))
        if 'car' in data and 'color' in data['car'] and data['car']['color'] == self.car_color:
            if 'raceTime' in data and 'laps' in data['raceTime']:
                self.lap = data['raceTime']['laps']
                if self.verbose:
                    print("CURRENT LAP: %s" % (self.lap))
                self.crashes_per_lap = 0
            if 'lapTime' in data and 'millis' in data['lapTime']:
                self.lap_time = data['lapTime']['millis'] / 1000

    def on_error(self, data):
        print("Error: %s" % (data))

    def on_finish(self, data):
        pass

    def write_log_to_file(self):
        with open(self.log_file, 'w') as f:
            f.write(json.dumps(self.log))

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finished,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'gameEnd': self.on_game_end,
            'raceSession': self.on_race_session,
            'error': self.on_error,
            'finish': self.on_finish,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            try:
                msg = json.loads(line)
                self.message_stack.append(msg)
                msg_type, data = msg['msgType'], msg['data']

                if self.write_log:
                    self.log.append(msg)

                if self.debug:
                    print ("MSG: %s" % (msg))
                if 'gameTick' in msg:
                    self.prev_tick = self.curr_tick
                    self.curr_tick = msg['gameTick']
                    self.tick_stack.append(self.curr_tick)
                    self.tick_stack = self.tick_stack[-self.STACK_SIZE:]
                    if self.curr_tick > self.turbo_end and self.turbo_end > 0:
                        print ("TURBO EXPIRED")
                        self.turbo_end = -1
                        self.turbo_on = False

                if msg_type in msg_map:
                    _timer = timer.Timer()
                    msg_map[msg_type](data)
                    _timer.end(limit=1 / 60)
                    if self.debug:
                        print(_timer.to_string())
                else:
                    print("Got unhandled {0}".format(msg_type))

                if self.message_sent_tick != self.curr_tick and msg_type != 'finish' and msg_type != 'crash' and msg_type != 'spawn' and 'turbo' not in msg_type:
                    self.ping()
            except:
                print("!!! Got unhandled exception: %s" % (sys.exc_info()[0]))
                traceback.print_exc()
                self.throttle(0.3)
            line = socket_file.readline()

        if self.verbose:
            print("Timing stats: %s" % timer.Timer().stats())
        print("Exit from loop")

        if self.write_log:
            self.write_log_to_file()


def setProxy(conn_sock, proxy_data):
    """ Parse and set proxy for socket

    >>> class s:
    ...   def __init__(self):
    ...     self.host = None
    ...     self.port = None
    ...   def setproxy(self, a, b, c):
    ...     self.host = b
    ...     self.port = c
    ...     pass
    ...
    >>> obj = s()
    >>> setProxy(obj, None)
    False
    >>> setProxy(obj, "")
    False
    >>> setProxy(obj, "123")
    False
    >>> setProxy(obj, "123:80")
    Setting proxy: 123 80
    True
    >>> obj.host
    '123'
    >>> obj.port
    80
    >>> setProxy(obj, "http://test.com:8080")
    Setting proxy: test.com 8080
    True
    >>> obj.host
    'test.com'
    >>> obj.port
    8080
    """
    if type(proxy_data) != str:
        return False

    proxy_data = proxy_data.replace("http://", "")
    proxy_data = proxy_data.replace("https://", "")
    if ":" not in proxy_data:
        return False

    datas = proxy_data.split(":")
    if len(datas) != 2:
        return False
    proxy_host = datas[0]
    proxy_port = int(datas[1])
    print("Setting proxy: %s %s" % (proxy_host, proxy_port))
    conn_sock.setproxy(socks.PROXY_TYPE_HTTP, proxy_host, proxy_port)
    return True

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        trackinfo = {}
        index = 5
        if len(sys.argv) >= (index + 4):
            trackinfo["start"] = sys.argv[index]
            index += 1
            trackinfo["track_name"] = sys.argv[index]
            index += 1
            trackinfo["pass"] = sys.argv[index]
            index += 1
            trackinfo["cars"] = sys.argv[index]

        #print trackinfo
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        conn_sock = socks.socksocket(socket.AF_INET, socket.SOCK_STREAM)
        if "http_proxy" in os.environ:
            setProxy(conn_sock, os.environ["http_proxy"])
        conn_sock.connect((host, int(port)))
        bot = Ecar42Bot(conn_sock, name, key)
        bot.run(trackinfo)

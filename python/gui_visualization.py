#!/usr/bin/env python

from __future__ import division
from __future__ import print_function
from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4.Qt import Qt
import json
import math
import parse_log
import sys

def parseLog(fname):
    with open(fname, 'r') as fd:
        return json.load(fd)

    return None

class TrackView(QtGui.QWidget):
    def __init__(self, car, startpos, parser, parent):
        super(TrackView, self).__init__(parent)
        self.car = car
        self.startpos = startpos
        self.parser = parser
        self.trackdata = parser.trackParser
        self.scale = 0.25
        self.move = False
        self.prev_pos = QtCore.QPointF(0, 0)
        self.pos = QtCore.QPointF(0, 0)
        self.clickpos = None
        self.carpos = 0

    def setCarPos(self, carpos):
        self.carpos = carpos
        self.update()

    def adaptToAngle(self, point, angle, length=1):
        x = point.x()
        y = point.y()
        r = 1

        angle_rad = angle * math.pi / 180
        ax = x + r * math.sin(angle_rad + length / r) - r * math.sin(angle_rad)
        ay = y - r * math.cos(angle_rad + length / r) + r * math.cos(angle_rad)
        return QtCore.QPointF(ax, ay)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.prev_pos = self.pos
            self._orig_pos = event.posF()
            self.move = True

    def mouseReleaseEvent(self, event):
        if self.move:
            self.pos = self.prev_pos + event.posF() - self._orig_pos
            self.prev_pos = self.pos
            self.move = False

    def mouseMoveEvent(self, event):
        if self.move:
            self.pos = self.prev_pos + event.posF() - self._orig_pos
            self.update()

    def wheelEvent(self, event):
        degrees = event.delta() / 8
        steps = degrees / 15
        self.scale += steps / 100
        if self.scale < 0.01:
            self.scale = 0.01
        self.update()
        event.accept()

    def paintEvent(self, event):
        paint = QtGui.QPainter()

        paint.begin(self)
        paint.fillRect(0, 0, self.width(), self.height(), QtGui.QColor(0, 255, 0))
        paint.setPen(QtGui.QColor(0, 0, 0))

        x = self.startpos['x']
        y = self.startpos['y']
        origox = self.width() / 2
        origoy = self.height() / 2
        origo = QtCore.QPointF(origox, origoy)

        angle = self.startpos['angle']
        first = True
        posdata = self.parser.get_data(self.carpos)
        for curpos, piece in enumerate(self.trackdata.pieces):
            if first:
                first = False
                paint.setPen(QtGui.QColor(0, 0, 255))

                length = 20
                startpoint = QtCore.QPointF(x, y - length) * self.scale + self.pos
                angle_rad = angle * math.pi / 180

                ax = x
                ay = y
                endpoint = QtCore.QPointF(ax, ay + length) * self.scale + self.pos

                paint.drawLine(startpoint + origo, endpoint + origo)

            draw_car = False
            if posdata is not None and curpos == posdata["pieceIndex"]:
                size_val = max(self.car["width"], self.car["length"]) * 2
                size_pos = QtCore.QPointF(size_val / 2, size_val / 2)

                car_size = QtCore.QSize(size_val, size_val)

                car_image = QtGui.QImage(car_size, QtGui.QImage.Format_ARGB32)
                car_image.fill(0)

                img_painter = QtGui.QPainter()
                img_painter.begin(car_image)
                img_painter.translate(size_pos)
                img_painter.rotate(posdata["angle"] + angle)

                car_apos = QtCore.QPointF(-self.car["width"] / 2, -self.car["guideFlagPosition"])
                car_asize = QtCore.QSizeF(self.car["width"], self.car["length"])
                rect = QtCore.QRectF(car_apos, car_asize)

                img_painter.fillRect(rect, QtGui.QColor(0, 0, 0))
                img_painter.end()

                car_mappos = (QtCore.QPointF(ax, ay) - size_pos) * self.scale + origo + self.pos
                scaled_size = QtCore.QSize(size_val, size_val) * self.scale

                draw_car = True

            if 'length' in piece:
                paint.setPen(QtGui.QColor(0, 0, 0))
                #print ("line from %s %s" % (x, y));
                startpoint = QtCore.QPointF(x, y) * self.scale + self.pos

                angle_rad = angle * math.pi / 180
                length = piece["length"]
                ax = x + math.sin(angle_rad) * length
                ay = y - math.cos(angle_rad) * length
                x = ax
                y = ay
                #print ("lineto %s %s, %s" % (x, y, angle));

                endpoint = QtCore.QPointF(x, y) * self.scale + self.pos
                paint.drawLine(startpoint + origo, endpoint + origo);
            elif "radius" in piece and "angle" in piece:
                length = abs(piece["radius"]) * abs(piece["angle"]) * math.pi / 180
                rect = QtCore.QRectF(x + origox, y + origoy, piece["radius"], piece["radius"])

                angle_rad = (angle - 90) * math.pi / 180
                r = piece["radius"]
                now_length = 0
                while now_length < length:
                    apen = QtGui.QPen(QtGui.QColor(255, 0, 0))
                    apen.setWidth(5)
                    paint.setPen(apen)
                    if piece["angle"] > 0:
                        ax = x + r * math.sin(angle_rad + now_length / r) - r * math.sin(angle_rad)
                        ay = y - r * math.cos(angle_rad + now_length / r) + r * math.cos(angle_rad)
                    else:
                        ax = x - r * math.sin(angle_rad - now_length / r) + r * math.sin(angle_rad)
                        ay = y + r * math.cos(angle_rad - now_length / r) - r * math.cos(angle_rad)
                    pos = QtCore.QPointF(ax, ay) * self.scale
                    pos += origo + self.pos
                    paint.drawPoint(pos)
                    now_length += 1

                #print ("arc from %s %s, %s" % (x, y, piece["angle"]));
                x = ax
                y = ay
                angle += piece["angle"]
                #print ("arc to %s %s" % (x, y));
                paint.setPen(QtGui.QColor(0, 0, 0))

            if draw_car:
                apen = QtGui.QPen(QtGui.QColor(0, 0, 0))
                apen.setWidth(2)
                paint.setPen(apen)

                paint.drawImage(car_mappos, car_image.scaled(scaled_size))
                draw_car = False

        paint.end()

class DataGraph(QtGui.QWidget):
    def __init__(self, dataset, parent):
        super(DataGraph, self).__init__(parent)
        self.dataset = dataset
        self.highlight = -1
        self.highlight_full = False

        self.zero_color = QtGui.QColor(255, 0, 0)
        self.graph_color = QtGui.QColor(0, 0, 255)
        self.graph_neg_color = QtGui.QColor(255, 0, 0)

        self.highlight_color = QtGui.QPen(QtGui.QColor(0, 255, 0))
        self.highlight_color.setWidth(2)
        self.show_zero = True
        self.scale = 1.0
        self.move = False
        self.prev_pos = QtCore.QPointF(0, 0)
        self.pos = QtCore.QPointF(0, 0)

        self.start_select = None
        self.end_select = None

    def setHighlightPos(self, pos):
        self.highlight = pos
        self.update()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton and event.modifiers() == Qt.ShiftModifier:
            self.start_select = event.posF()
        elif event.button() == Qt.LeftButton:
            self.prev_pos = self.pos
            self._orig_pos = event.posF()
            self.move = True

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton and event.modifiers() == Qt.ShiftModifier:
            if self.start_select is not None:
                a = (self.start_select / self.scale - self.pos).x()
                b = (event.posF() / self.scale - self.pos).x()
                items = len(self.dataset)
                xinc = (self.width() - 5) / items
                print (a, b, xinc)
                a /= xinc
                b /= xinc
                print (a, b)
                if a > b:
                    tmp = a
                    a = b
                    b = tmp
                a = int(a)
                b = int(b)
                a = max(0, a)
                b = max(0, b)
                if a != b and b < items:
                    print (self.dataset[a:b])

            self.start_select = None
        if self.move:
            self.pos = self.prev_pos + event.posF() - self._orig_pos
            self.prev_pos = self.pos
            self.move = False

    def mouseMoveEvent(self, event):
        if self.start_select is not None and event.modifiers() == Qt.ShiftModifier:
            self.end_select = event.posF()
            self.update()
        if self.move:
            self.pos = self.prev_pos + event.posF() - self._orig_pos
            self.update()

    def wheelEvent(self, event):
        degrees = event.delta() / 8
        steps = degrees / 15
        self.scale += steps / 25
        if self.scale < 0.01:
            self.scale = 0.01
        self.update()
        event.accept()

    def paintEvent(self, event):
        if not self.dataset:
            return

        paint = QtGui.QPainter()
        paint.begin(self)
        x = 0.0
        y = self.height()
        maxpos = len(self.dataset)
        xinc = (self.width() - 5) / maxpos
        max_data = max(self.dataset)
        min_data = min(self.dataset)
        if min_data > 0.0:
            min_data = 0.0

        pos = 0
        scale = self.height() / (max_data - min_data)
        zeropoint = self.height() + min_data * scale

        for acc in self.dataset:
            if pos == self.highlight:
                paint.setPen(self.highlight_color)
                if self.highlight_full:
                    startpoint = QtCore.QPointF(x, 0) + self.pos
                    endpoint = QtCore.QPointF(x, self.height()) + self.pos
                    startpoint *= self.scale
                    endpoint *= self.scale
                    paint.drawLine(startpoint, endpoint)
            else:
                if acc < 0:
                    paint.setPen(self.graph_neg_color)
                else:
                    paint.setPen(self.graph_color)

            startpoint = QtCore.QPointF(x, (zeropoint - acc * scale)) + self.pos
            endpoint = QtCore.QPointF(x, zeropoint) + self.pos
            startpoint *= self.scale
            endpoint *= self.scale
            paint.drawLine(startpoint, endpoint)

            x += xinc
            pos += 1

        if self.show_zero:
            paint.setPen(QtGui.QColor(self.zero_color))

            startpoint = QtCore.QPointF(0, zeropoint) + self.pos
            endpoint = QtCore.QPointF(xinc * maxpos, zeropoint) + self.pos
            startpoint *= self.scale
            endpoint *= self.scale
            paint.drawLine(startpoint, endpoint)

        if self.start_select is not None and self.end_select is not None:
            paint.fillRect(QtCore.QRectF(QtCore.QPointF(self.start_select.x(), 0), QtCore.QPointF(self.end_select.x(), self.height())), QtGui.QColor(0, 0, 0, 100))

        paint.end()

class AccelerationGraph(DataGraph):
    def __init__(self, dataset, parent):
        super(AccelerationGraph, self).__init__(dataset, parent)
        self.highlight_full = True

class ThrottleGraph(DataGraph):
    def __init__(self, dataset, parent):
        super(ThrottleGraph, self).__init__(dataset, parent)
        self.show_zero = False
        self.highlight_color = QtGui.QPen(QtGui.QColor(255, 0, 0))
        self.highlight_color.setWidth(2)

class AngleGraph(DataGraph):
    def __init__(self, dataset, parent):
        super(AngleGraph, self).__init__(dataset, parent)
        self.show_zero = True
        self.highlight_color = QtGui.QPen(QtGui.QColor(255, 0, 0))
        self.highlight_color.setWidth(2)
        #self.graph_neg_color = self.graph_color
        self.highlight_full = True

class LogViewer(QtGui.QWidget):
    def __init__(self, data):
        super(LogViewer, self).__init__()
        self.gameTick = 0
        self._layout = QtGui.QVBoxLayout()
        self._button_layout = QtGui.QHBoxLayout()

        self.log = data
        self.car = {}
        self.car_color = None
        self.startpos = {'x': 0, 'y': 0, 'angle': 0}
        self.trackparser = parse_log.trackParser()

        self.initUI()
        self.parser = parse_log.ecar42Parser(self.trackparser)
        self.setLayout(self._layout)

        self.parse()

        self.completeUI()

    def initUI(self):
        self.resize(800, 600)
        self.setWindowTitle('HWO Data Visualization')

        self._layout.addLayout(self._button_layout)

        self._map_box = QtGui.QCheckBox("Map")
        self._accelerate_box = QtGui.QCheckBox("Accelerate")
        self._throttle_box = QtGui.QCheckBox("Throttle")
        self._speed_box = QtGui.QCheckBox("Speed")
        self._target_speed_box = QtGui.QCheckBox("Target speed")
        self._angle_box = QtGui.QCheckBox("Angle")

        self._map_box.setCheckState(Qt.Checked)
        self._throttle_box.setCheckState(Qt.Checked)

        self._button_layout.addWidget(self._map_box)
        self._button_layout.addWidget(self._accelerate_box)
        self._button_layout.addWidget(self._throttle_box)
        self._button_layout.addWidget(self._speed_box)
        self._button_layout.addWidget(self._target_speed_box)
        self._button_layout.addWidget(self._angle_box)

        self.slider = QtGui.QSlider(Qt.Horizontal)
        self._layout.addWidget(self.slider)

    def completeUI(self):
        self.slider.setMaximum(self.gameTick)
        self.slider.setValue(0)

        self.slider.connect(self.slider, QtCore.SIGNAL("valueChanged(int)"), self.posChanged)
        self.acceleration = AccelerationGraph(self.parser.acceleration, self)
        self._layout.addWidget(self.acceleration)

        self.throttle = ThrottleGraph(self.parser.throttle, self)
        self._layout.addWidget(self.throttle)

        self.speed = DataGraph(self.parser.speed, self)
        self._layout.addWidget(self.speed)

        self.target_speed = DataGraph(self.parser.targetSpeed, self)
        self._layout.addWidget(self.target_speed)

        self.angle = AngleGraph(self.parser.get_key_datas("angle"), self)
        self._layout.addWidget(self.angle)

        self.acceleration.hide()
        self.target_speed.hide()
        self.speed.hide()
        self.angle.hide()

        QtCore.QObject.connect(self._map_box, QtCore.SIGNAL("stateChanged(int)"), self.mapChanged)
        QtCore.QObject.connect(self._accelerate_box, QtCore.SIGNAL("stateChanged(int)"), self.accelerateChanged)
        QtCore.QObject.connect(self._throttle_box, QtCore.SIGNAL("stateChanged(int)"), self.throttleChanged)
        QtCore.QObject.connect(self._speed_box, QtCore.SIGNAL("stateChanged(int)"), self.speedChanged)
        QtCore.QObject.connect(self._target_speed_box, QtCore.SIGNAL("stateChanged(int)"), self.targetSpeedChanged)
        QtCore.QObject.connect(self._angle_box, QtCore.SIGNAL("stateChanged(int)"), self.angleChanged)

    def mapChanged(self):
        self.track_view.setVisible(self._map_box.checkState() == Qt.Checked)

    def accelerateChanged(self):
        self.acceleration.setVisible(self._accelerate_box.checkState() == Qt.Checked)

    def throttleChanged(self):
        self.throttle.setVisible(self._throttle_box.checkState() == Qt.Checked)

    def speedChanged(self):
        self.speed.setVisible(self._speed_box.checkState() == Qt.Checked)

    def targetSpeedChanged(self):
        self.target_speed.setVisible(self._target_speed_box.checkState() == Qt.Checked)

    def angleChanged(self):
        self.angle.setVisible(self._angle_box.checkState() == Qt.Checked)

    def parse(self):
        for data in self.log:
            if type(data) != dict:
                continue
            if "data" not in data:
                continue
            if data["data"] is None:
                continue

            if "race" in data["data"]:
                self.parseRace(data["data"]["race"])
            elif data["msgType"] == "yourCar" and "color" in data["data"]:
                self.car_color = data["data"]["color"]
            elif data["msgType"] == "carPositions":
                self.parseCarPositions(data)
            elif data["msgType"] == "throttleData":
                self.parser.update_throttle(data["data"])
            elif data["msgType"] == "targetSpeedInfo":
                self.parser.update_target_speed(data["data"])

    def posChanged(self, val):
        self.track_view.setCarPos(val)
        self.acceleration.setHighlightPos(val)
        self.throttle.setHighlightPos(val)
        self.speed.setHighlightPos(val)
        self.target_speed.setHighlightPos(val)
        self.angle.setHighlightPos(val)

    def parseCarPositions(self, data):
        if "gameTick" in data:
            self.gameTick =  data["gameTick"]

        for car in data["data"]:
            if "id" in car and "color" in car["id"] and car["id"]["color"] == self.car_color:
                self.parser.update_data(car, self.gameTick)

    def parseRace(self, data):
        if "cars" in data:
            self.parseCars(data["cars"])
        if "track" in data:
            self.parseTrack(data["track"])

    def parseCars(self, data):
        for car in data:
            if car["id"]["color"] == self.car_color:
                self.car = car["dimensions"]
                self.car["color"] = self.car_color

    def parseTrack(self, data):
        self.trackparser.set_track(data, do_print=False)
        if "startingPoint" in data:
            self.startpos = self.parseStartingPoint(data["startingPoint"])
        self.track_view = TrackView(self.car, self.startpos, self.parser, self)
        self._layout.addWidget(self.track_view)

    def parseStartingPoint(self, data):
        y = 0
        x = 0
        angle = 0
        if  "position" in data:
            if 'y' in data["position"]:
                y = data["position"]["y"]
            if 'x' in data["position"]:
                x = data["position"]["x"]
        if "angle" in data:
            angle = data["angle"]

        return {"x": x, "y": y, "angle": angle}


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print ("Usage: %s logfile.json" % (sys.argv[0]))
        sys.exit(1)

    log = parseLog(sys.argv[1])

    app = QtGui.QApplication(sys.argv)
    win = LogViewer(log)
    win.show()
    sys.exit(app.exec_())

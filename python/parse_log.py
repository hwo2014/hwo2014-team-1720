#!/usr/bin/python

import sys
import json
import math

class trackParser():
    def __init__(self):
        self.name   = ''
        self.lanes  = []
        self.pieces = []

    def set_track(self, track_data, do_print=True):
        self.name   = track_data['name']
        self.lanes  = track_data['lanes']
        self.pieces = track_data['pieces']
        if do_print:
            self.print_track()

    def get_piece_length(self, pieceIndex, lane):
        angle = self.get_piece_angle(pieceIndex)
        if angle == 0:
            return self.pieces[pieceIndex]['length']
        radius = self.get_radius_for_lane(pieceIndex, lane)
        return 2 * math.pi * radius * abs(angle) / 360

    def get_radius_for_lane(self, pieceIndex, lane):
        centerRadius        = self.pieces[pieceIndex]['radius']
        distanceFromCenter  = self.lanes[lane]['distanceFromCenter']
        angle = self.get_piece_angle(pieceIndex)
        if angle > 0:
            return centerRadius - distanceFromCenter
        return centerRadius + distanceFromCenter

    def get_piece_angle(self, pieceIndex):
        if not 'angle' in self.pieces[pieceIndex].keys():
            return 0
        else:
            return self.pieces[pieceIndex]['angle']

    def print_track(self):
        i = 0
        for piece in self.pieces:
            print i
            i += 1
            if 'switch' in piece.keys():
                print 'Switch',
            if 'angle' in piece.keys():
                if piece['angle'] > 0:
                    print 'Right turn %s %s' % (piece['radius'], piece['angle'])
                else:
                    print 'Left turn %s %s' % (piece['radius'], piece['angle'])
            else:
                print 'Straight %s' % piece['length']

class ecar42Parser():
    def __init__(self, trackparser):
        self.trackParser = trackparser
        self.data = []
        self.throttle = []
        self.targetSpeed = []
        self.speed = []
        self.acceleration = []

    def update_throttle(self, data):
        self.throttle.append(data['throttle'])

    def update_target_speed(self, data):
        self.targetSpeed.append(data['targetSpeed'])

    def update_data(self, data, gameTick):
        tmp = {}
        tmp["gameTick"] = gameTick
        tmp["pieceIndex"]         = data['piecePosition']['pieceIndex']
        tmp["startLineIndex"]     = data['piecePosition']['lane']['startLaneIndex']
        tmp["endLineIndex"]       = data['piecePosition']['lane']['endLaneIndex']
        tmp["lap"]                = data['piecePosition']['lap']
        tmp["inPieceDistance"]    = data['piecePosition']['inPieceDistance']
        tmp["angle"]              = data['angle']
        self.data.append(tmp)

        self.calculate_data()

    def get_data(self, tick):
        for val in self.data:
            if val["gameTick"] == tick:
                return val
        return None

    def get_key_datas(self, key):
        res = []
        for val in self.data:
            if key not in val:
                return None

            res.append(val[key])

        return res

    def calculate_data(self):
        if len(self.data) < 2:
            return
        if self.data[-1]["gameTick"] == self.data[-2]["gameTick"]:
            return
        self.calculate_speed()
        self.calculate_acceleration()

    def calculate_acceleration(self):
        if len(self.data) < 2:
            return
        if len(self.speed) < 2:
            return
        self.acceleration.append((self.speed[-1] - self.speed[-2]) / (self.data[-1]["gameTick"] - self.data[-2]["gameTick"]))

    def calculate_speed(self):
        if len(self.data) < 2:
            return
        if self.data[-1]["pieceIndex"] == self.data[-2]["pieceIndex"] + 1 or self.data[-1]["lap"] > self.data[-2]["lap"]:
            speed_len = (self.data[-1]["inPieceDistance"] + (self.trackParser.get_piece_length(self.data[-2]["pieceIndex"], self.data[-1]["startLineIndex"]) - self.data[-2]["inPieceDistance"]))
            speed_time = (self.data[-1]["gameTick"] - self.data[-2]["gameTick"])
            self.speed.append(speed_len / speed_time)
        else:
            self.speed.append((self.data[-1]["inPieceDistance"] - self.data[-2]["inPieceDistance"]) / (self.data[-1]["gameTick"] - self.data[-2]["gameTick"]))

    def print_data(self):
        if self.data and self.speed and self.acceleration and self.throttle:
            if self.targetSpeed:
                targetspeed = self.targetSpeed[-1]
            else:
                targetspeed = 0
            print 'PieceIdx: %s, InpieceDist: %s, PieceLen: %s, Angle: %s, Speed: %s, Acc: %s, Throttle: %s, TargetSpeed %s' % \
                (self.data[-1]["pieceIndex"], \
                round(self.data[-1]["inPieceDistance"], 2), \
                round(self.trackParser.get_piece_length(self.data[-1]["pieceIndex"], self.data[-1]["startLineIndex"]), 2), \
                round(self.data[-1]["angle"], 2), \
                round(self.speed[-1], 2), \
                round(self.acceleration[-1], 4), \
                round(self.throttle[-1], 4), \
                round(targetspeed, 4))
            if self.speed[-1]:
                print ("Centri: %s" % (self.speed[-1] ** 2 / 90))

if __name__ == '__main__':
    with open(sys.argv[1], 'r') as f:
        messages = json.load(f)

    t = trackParser()
    e = ecar42Parser(trackparser=t)

    gameTick = 0
    for m in messages:
        if m['msgType'] == 'gameInit':
            t.set_track(m['data']['race']['track'])
        elif m['msgType'] == 'carPositions':
            if 'gameTick' in m.keys():
                gameTick = m['gameTick']
            e.update_data(m['data'][0], gameTick)
            e.print_data()
        elif m['msgType'] == 'throttleData':
            e.update_throttle(m['data'])
        elif m['msgType'] == 'targetSpeedInfo':
            e.update_target_speed(m['data'])

#!/usr/bin/env python

from __future__ import division
from __future__ import print_function
import math

""" Pacejka's algorithm, idea and implementation details stolen from:
    http://www.racer.nl/reference/pacejka.htm
    http://en.wikipedia.org/wiki/Hans_B._Pacejka#The_Pacejka_.22Magic_Formula.22_tire_models
"""


class Pacejka:
    def __init__(self):
        """ Initialize with empty values

        >>> obj = Pacejka()
        >>> obj.load
        0
        >>> obj.force_lat
        0
        >>> obj.force_long
        0
        >>> len(obj.a)
        17
        >>> len(obj.b)
        11
        >>> len(obj.c)
        18
        """
        self.camber = 0
        self.slip_angle = 0
        self.slip_percentage = 0

        self.force_long = 0
        self.force_long_max = 0
        self.force_lat = 0
        self.force_lat_max = 0

        self.stiffness_long = 0
        self.stiffness_lat = 0

        self.aligning_moment = 0

        self.load = 0
        self.a = dict.fromkeys(xrange(14 + 1), 0)
        self.a[111] = 0
        self.a[112] = 0
        self.b = dict.fromkeys(xrange(10 + 1), 0)
        self.c = dict.fromkeys(xrange(17 + 1), 0)

        self.initValues()

    def initValues(self):
        """ Set some good guess initial values

        >>> obj = Pacejka()
        >>> obj.a[0]
        1.4
        >>> obj.b[0]
        1.65
        >>> obj.c[0]
        2.46
        """
        # Default values
        self.a[0] = 1.4 # Maybe not that important?
        self.a[1] = 0
        self.a[2] = 1100 # Major tune
        self.a[3] = 1100 # Minor tune?
        self.a[4] = 50 # Major tune
        self.a[5] = 0
        self.a[6] = 0
        self.a[7] = -2
        self.a[8] = 0
        self.a[9] = 0
        self.a[10] = 0
        self.a[11] = 0
        self.a[12] = 0
        self.a[13] = 0
        self.a[111] = 0
        self.a[112] = 0

        self.b[0] = 1.65
        self.b[1] = 0
        self.b[2] = 1668
        self.b[3] = 0
        self.b[4] = 229
        self.b[5] = 0
        self.b[6] = 0
        self.b[7] = 0
        self.b[8] = -10
        self.b[9] = 0
        self.b[10] = 0

        """
        # Other example values
        self.a[0] = 1.3
        self.a[1] = -49
        self.a[2] = 1216
        self.a[3] = 1632
        self.a[4] = 11
        self.a[5] = 0.006
        self.a[6] = -0.04
        self.a[7] = -0.4
        self.a[8] = 0.003
        self.a[9] = -0.002
        self.a[10] = 0
        self.a[11] = 0
        self.a[12] = 0
        self.a[13] = 0
        self.a[111] = -11
        self.a[112] = 0.045

        self.b[0] = 1.57
        self.b[1] = -48
        self.b[2] = 1338
        self.b[3] = 6.8
        self.b[4] = 444
        self.b[5] = 0
        self.b[6] = 0.0034
        self.b[7] = -0.008
        self.b[8] = 0.66
        self.b[9] = 0
        self.b[10] = 0
        """

        self.c[0] = 2.46
        self.c[1] = -2.77
        self.c[2] = -2.9
        self.c[3] = 0
        self.c[4] = -3.6
        self.c[5] = -0.1
        self.c[6] = 0.0004
        self.c[7] = 0.22
        self.c[8] = -2.31
        self.c[9] = 3.87
        self.c[10] = 0.0007
        self.c[11] = -0.05
        self.c[12] = -0.006
        self.c[13] = 0.33
        self.c[14] = -0.04
        self.c[15] = -0.4
        self.c[16] = 0.092
        self.c[17] = 0.0114

    def setSlipRatio(self, ratio):
        """ Sets the slip ratio for longitudinal force

        >>> obj = Pacejka()
        >>> obj.slip_percentage
        0
        >>> obj.setSlipRatio(1)
        >>> obj.slip_percentage
        100
        >>> obj.setSlipRatio(0.42)
        >>> obj.slip_percentage
        42.0
        """
        self.slip_percentage = ratio * 100

    def setSlipAngleInDegrees(self, angle):
        """ Sets slip angle in degrees

        >>> obj = Pacejka()
        >>> obj.slip_angle
        0
        >>> obj.setSlipAngleInDegrees(10)
        >>> obj.slip_angle
        10
        >>> obj.setSlipAngleInDegrees(45)
        >>> obj.slip_angle
        45
        """
        self.slip_angle = angle
        #self.slip_angle = angle * math.pi / 180

    def setSlipAngleInRadians(self, angle):
        """ Sets slip angle in radians

        >>> obj = Pacejka()
        >>> obj.slip_angle
        0
        >>> obj.setSlipAngleInRadians(0.17453292519943295)
        >>> obj.slip_angle
        10.0
        >>> obj.setSlipAngleInRadians(0.7853981633974483)
        >>> obj.slip_angle
        45.0
        """
        self.slip_angle = angle * 180 / math.pi

    def setLoad(self, load):
        """ Sets the load per tyre

        >>> obj = Pacejka()
        >>> obj.load
        0
        >>> obj.setLoad(10)
        >>> obj.load
        0.01
        >>> obj.setLoad(42)
        >>> obj.load
        0.042
        >>> obj.setLoad(4200)
        >>> obj.load
        4.2
        """
        self.load = load / 1000

    def setMass(self, total_mass):
        """ Set mass of car, calculate load per tyre (4 tyres)

        >>> obj = Pacejka()
        >>> obj.load
        0
        >>> obj.setMass(10)
        >>> obj.load # doctest: +ELLIPSIS
        0.024...
        >>> obj.setMass(42)
        >>> obj.load # doctest: +ELLIPSIS
        0.10300...
        >>> obj.setMass(4200)
        >>> obj.load
        10.3005
        >>> obj.setMass(407.7472)
        >>> obj.load # doctest: +ELLIPSIS
        1.00...
        """
        self.setLoad(total_mass * 9.81 / 4)

    def calculate(self):
        """ Do the calculations
        """
        self.calculateLong()
        self.calculateLat()
        self.calculateAligningMoment()

    def nearZero(self, value):
        """ Check if value is near zero

        >>> obj = Pacejka()
        >>> obj.nearZero(1)
        False
        >>> obj.nearZero(0.001)
        False
        >>> obj.nearZero(0.0001)
        False
        >>> obj.nearZero(0.00001)
        False
        >>> obj.nearZero(0.000001)
        True
        >>> obj.nearZero(0.000005)
        True
        >>> obj.nearZero(0.000009999999)
        True
        """
        return abs(value) < 0.00001

    def calculateLong(self):
        """ Calculate longitudinal force, ie. Fx
        """
        C = self.b[0]
        uP = self.b[1] * self.load + self.b[2]
        D = uP * self.load

        if self.nearZero(C) or self.nearZero(D):
            B = 99999
        else:
            B = ((self.b[3] * self.load ** 2 + self.b[4] * self.load) * math.exp(-self.b[5] * self.load)) / (C * D)

        E = self.b[6] * self.load ** 2 + self.b[7] * self.load + self.b[8]
        Sh = self.b[9] * self.load + self.b[10]
        Sv = 0

        self.stiffness_long = B * C * D
        self.force_long_max = D + Sv

        self.force_long = D * math.sin(C * math.atan(B * (1.0 - E) * (self.slip_percentage + Sh) + E * math.atan(B * (self.slip_percentage + Sh)))) + Sv

    def calculateLat(self):
        """ Calculate lateral force, ie. Fy
        """
        C = self.a[0]
        uP = self.a[1] * self.load + self.a[2]
        D = uP * self.load
        E = self.a[6] * self.load + self.a[7]

        if self.nearZero(C) or self.nearZero(D) or self.nearZero(self.a[4]):
            B = 99999
        else:
            self.stiffness_lat = self.a[3] * math.sin(2 * math.atan(self.load / self.a[4])) * (1 - self.a[5] * abs(self.camber))
            B = self.stiffness_lat / (C * D)

        Sh = self.a[8] * self.camber + self.a[9] * self.load + self.a[10]
        Sv = (self.a[111] * self.load + self.a[112]) * self.camber * self.load + self.a[12] * self.load + self.a[13]

        self.force_lat_max = D + Sv

        self.force_lat = D * math.sin(C * math.atan(B * (1 - E) * (self.slip_angle + Sh) + E * math.atan(B * (self.slip_angle + Sh)))) + Sv

    def calculateAligningMoment(self):
        """ Calculate aligning moment, ie. Fz
        """
        C = self.c[0]
        D = self.c[1] * self.load ** 2 + self.c[2] * self.load
        E = (self.c[7] * self.load ** 2 + self.c[8] * self.load + self.c[9]) * (1 - self.c[10] * abs(self.camber))

        if self.nearZero(C) or self.nearZero(D):
            B = 99999
        else:
            B = ((self.c[3] * self.load ** 2 + self.c[4] * self.load) * (1 - self.c[6] * abs(self.camber)) * math.exp(-self.c[5] * self.load)) / (C * D)

        Sh = self.c[11] * self.camber + self.c[12] * self.load + self.c[13]
        Sv = (self.c[14] * self.load ** 2 + self.c[15] * self.load) * self.camber + self.c[16] * self.load + self.c[17]

        self.aligning_moment = D * math.sin(C * math.atan(B * (1 - E) * (self.slip_angle + Sh) + E * math.atan(B * (self.slip_angle + Sh)))) + Sv

    def maxForce(self):
        """ Get the maximum load force
        """
        return (self.b[1] * self.load + self.b[2]) * self.load

if __name__ == '__main__':
    pace = Pacejka()

    # Some other defaults
    #pace.setLoad(5 * 9.8 / 4)
    pace.setLoad(5 * 9.8)
    #pace.setMass(5.0)

    res = []
    angle_values = [-0.061152217549847426, 0.019533343301639763, 0.09194129018865652, 0.1561242716469643, 0.21221748464377532, 0.260429020630404, 0.30103039349853633, 0.3343473322430756, 0.3607509101392991, 0.38064907156590205, 0.3944786073307542, 0.4026976195528292, 0.4057785078798848, 0.40420150012381584, 0.39844874231018573, 0.7654331139153536, 1.4643481657675697, 2.4548986796446735, 3.6976807288207496, 5.154514416202512, 6.788730434854368, 8.565411975229088, 10.451593829137686, 12.416420814238094, 14.431267866803587, 16.469824327269293, 18.50814507532626, 20.52467126211665, 22.500223439510197, 24.417969903742193, 26.263373056154993, 28.024116540712757, 29.690015849636602, 31.252914998163668, 32.70657176021565, 34.04653383170718, 35.27000815023924, 36.37572545176571, 37.363801989085744, 38.23560017612096, 38.99358975811773, 39.641210943217494, 40.18274076711287, 40.62316380141319, 40.96804815934543, 41.22342760079168, 41.3956903935056, 41.49147544957124, 41.131511297363325, 40.36521752621767, 39.241442678963665, 37.80799669712975]
    values = [math.sin(a * math.pi / 180) * 30 for a in angle_values]
    print (angle_values)
    print (values)
    """
    #ratio = -2.0
    ratio = 0
    while ratio <= 2:
        pace.setSlipRatio(ratio)
        pace.calculate()

        res.append(pace.force_long)
        print ("%s %s" % (ratio, pace.force_long))
        ratio += 0.01

    """
    #slip = -40
    slip = 0
    while slip <= 30:
        pace.setSlipAngleInDegrees(slip)
        #pace.setSlipAngleInRadians(slip)
        pace.calculate()

        res.append(pace.force_lat)
        #res.append(pace.aligning_moment)
        print ("%s %s %s %s" % (slip, pace.force_lat, pace.aligning_moment, pace.force_lat_max))
        slip += 0.1

    print (res)

    print (pace.maxForce())

from __future__ import division
from __future__ import print_function
import time

class Timer:
    __limits = 0
    __total = 0
    __total_cnt = 0

    def __init__(self):
        self.result = None
        self.start()

    def start(self):
        """ Start the timer
        >>> obj = Timer()
        >>> start1 = obj.start_time
        >>> start1 > 0
        True
        >>> obj.start()
        >>> obj.start_time > start1
        True
        """
        self.start_time = time.time()

    def end(self, limit=0.0):
        """ End taking time

        >>> obj = Timer()
        >>> obj.end()
        >>> obj.result > 0
        True
        >>> obj.result > 1/100
        False
        >>> obj.start()
        >>> time.sleep(1/1000)
        >>> obj.end()
        >>> obj.result > 1/1000
        True
        >>> obj.result < 2/1000
        True
        """
        self.result = time.time() - self.start_time
        self.limit = limit
        Timer.__total += self.result
        Timer.__total_cnt += 1
        if self.limit > 0.0 and self.result > self.limit:
            Timer.__limits += 1

    def to_string(self):
        """ Get string representaion of object

        >>> obj = Timer()
        >>> obj.to_string() # doctest: +ELLIPSIS
        Traceback (most recent call last):
        ...
        ValueError: TIMER: Time taking still going on, use Timer.end()
        >>> obj.end()
        >>> obj.to_string() # doctest: +ELLIPSIS
        'TIMER elapsed: ... milliseconds.'
        >>> obj.end(1/100000000)
        >>> obj.to_string() # doctest: +ELLIPSIS
        'TIMER elapsed: ... milliseconds. Limit (... milliseconds) hit!'
        """
        if self.result is None:
            raise ValueError("TIMER: Time taking still going on, use Timer.end()")

        data = self.result
        units = "seconds"
        limit = self.limit
        if data < 1.0:
            data *= 1000
            units = "milliseconds"
            limit *= 1000

        res = "TIMER elapsed: %s %s." % (data, units)
        if limit > 0.0 and data > limit:
            res += " Limit (%s %s) hit!" % (limit, units)

        return res

    def stats(self):
        """ Get stats of global timing

        >>> obj = Timer()
        >>> obj.end()
        >>> res = obj.stats()
        >>> res["limit_hit"]
        0
        >>> res["average"] > 0
        True
        >>> obj.reset_stats()
        >>> obj.start()
        >>> time.sleep(0.1)
        >>> obj.end()
        >>> obj.start()
        >>> time.sleep(0.1)
        >>> obj.end()
        >>> res = obj.stats()
        >>> res["average"] >= 0.1
        True
        >>> res["average"] < 0.15
        True
        >>> obj.start()
        >>> time.sleep(0.1)
        >>> obj.end()
        >>> res["limit_hit"]
        0
        >>> obj.end(0.01)
        >>> res = obj.stats()
        >>> res["limit_hit"]
        1
        >>> obj.end(0.01)
        >>> res = obj.stats()
        >>> res["limit_hit"]
        2
        """
        return {"limit_hit": Timer.__limits, "average": Timer.__total/Timer.__total_cnt}

    def reset_stats(self):
        """ Resets the global stats
        """

        Timer.__limits = 0
        Timer.__total = 0
        Timer.__total_cnt = 0

    def __repr__(self):
        return self.to_string()

    def __str__(self):
        return self.to_string()
